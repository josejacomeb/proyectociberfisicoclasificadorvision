#!/usr/bin/python3
# -*- coding: utf-8 -*-
# instalar pip install socketIO-client-2
from socketIO_client import SocketIO
from threading import Thread
import comserial
import json
socket = ""
modo = "cmnm"
bandeja1 = "Metal"
bandeja2 = "No metal"
inicio = False;

def on_connect():
    print ("Conectado al Servidor")

def on_disconnect():
    print ("Desconectado al Servidor")

def on_reconnect():
    print ("Reconectado al servidor")
def _receive_events_thread():
    global socket
    socket.wait()
def enviarvalores(identificador, diccionario):
    global socket
    socket.emit(identificador, diccionario)

def leer_valores(*args):
    salida = json.dumps(args)
    salida = json.loads(salida)
    salida = dict(salida[0])
    if salida["nombre"] == "Piston1":
        if salida["valor"]:
            if comserial.comunicacionactivada:
                comserial.enviardatos('n')
        else:
            if comserial.comunicacionactivada:
                comserial.enviardatos('o')
    elif salida["nombre"] == "Piston2":
        if salida["valor"]:
            if comserial.comunicacionactivada:
                comserial.enviardatos('p')
        else:
            if comserial.comunicacionactivada:
                comserial.enviardatos('q')
    elif salida["nombre"] == "velocidadBanda":
        if salida["valor"] == '0':
            if comserial.comunicacionactivada:
                comserial.enviardatos('3')
        elif salida["valor"] == '1':
            if comserial.comunicacionactivada:
                comserial.enviardatos('4')
        elif salida["valor"] == '2':
            if comserial.comunicacionactivada:
                comserial.enviardatos('5')
        elif salida["valor"] == '3':
            if comserial.comunicacionactivada:
                comserial.enviardatos('6')
        elif salida["valor"] == '4':
            if comserial.comunicacionactivada:
                comserial.enviardatos('7')
        elif salida["valor"] == '5':
            if comserial.comunicacionactivada:
                comserial.enviardatos('8')
def modooperacion(*args):
    salida = json.dumps(args)
    salida = json.loads(salida)
    salida = dict(salida[0])
    global modo, bandeja1, bandeja2
    if salida["nombre"] == "modo":
        modo = salida["modo"]
        print("Cambio de modo: ",modo)
        bandeja1 = salida["bandeja1"]
        bandeja2 = salida["bandeja2"]
def estadodispositivo(*args):
    print(*args)
    global inicio
    salida = json.dumps(args)
    salida = json.loads(salida)
    salida = dict(salida[0])
    global modo, bandeja1, bandeja2
    print(type(salida["estado"]))
    if salida["estado"] == 1:
        if comserial.comunicacionactivada:
            comserial.enviardatos('9')
    elif salida["estado"] == 0:
        if comserial.comunicacionactivada and inicio:
            comserial.enviardatos('.')
    elif salida["estado"] == -1:
        if comserial.comunicacionactivada and inicio:
            comserial.enviardatos('0')
    inicio = True

def inicializar(direccionip='http://localhost',puerto=3000):
    global socket
    print("Inicializando")
    socket = SocketIO(direccionip,int(puerto))
    socket.on('connect', on_connect)
    socket.on('disconnect', on_disconnect)
    socket.on('reconnect', on_reconnect)
    socket.on('valores', leer_valores)
    socket.on('modo', modooperacion)
    socket.on('estado', estadodispositivo)
    receive_events_thread = Thread(target=_receive_events_thread)
    receive_events_thread.daemon = True
    receive_events_thread.start()
