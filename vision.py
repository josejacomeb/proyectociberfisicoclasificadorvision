#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    Utiliza las librerias scipy  y base64 que son fuera del sistema
'''

__name__ = "vision"
#instalar scipy
import numpy as np
import cv2 as cv
import math
import base64
import platform
from scipy.spatial import distance as dist
alto =  480
ancho = 640
imagen = np.zeros((alto, ancho), np.uint8)
imagengris = np.zeros((alto, ancho), np.uint8)
imagenbinary = np.zeros((alto, ancho), np.uint8)
imagennormalizada = np.zeros((alto, ancho), np.uint8)
dstcolor = np.zeros((alto, ancho), np.uint8)
tipo = ""
roix1 = 187
roix2 = 540
ancholinea = 15
distanciaminimaesquinas = 600
areaminimacontorno = 0.007 #El area debe ser mayor del 1% del area total de la figura para ser considerada
areaminimaaceptacion = 0.03 #El area cubre el agujero de 1/4in con respecto a la area total del ROI
areafigura = 0
formacuadrada = True
valorsgris = 42
valorvblanco = 194
valorhverdeminimo = 51
valorhverdemaximo = 104
valorhamarillominimo = 21
valorhamarillomaximo = 48
errores = ""
color = ""
forma  = ""
def inicializarvision():
    pass

def preprocesamiento():
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    global imagenbinary, imagen, imagengris, imagennormalizada, roix1, roix2
    global alto, ancho
    alto = imagen.shape[1]
    ancho = imagen.shape[0]
    #Utiliza las variables globales
    imagen = imagen[0:alto, roix1:roix2] #Usando el constructor de Numpy
    if platform.machine() == 'x86_64':
        cv.imshow("Recortada",imagen)
    alto = imagen.shape[1]
    ancho = imagen.shape[0]
    imagengris = cv.cvtColor(imagen,cv.COLOR_BGR2GRAY)
    imagengris = cv.medianBlur(imagengris,5)
    imagennormalizada = np.zeros((imagengris.shape[0], imagengris.shape[1]), np.uint8)
    cv.normalize(imagengris, imagennormalizada, 0, 255, cv.NORM_MINMAX)
    retval,imagenbinary = cv.threshold(imagennormalizada,120,255,cv.THRESH_BINARY|cv.THRESH_OTSU)
    kernel = np.ones((5,5),np.uint8)
    imagenbinary = cv.erode(imagenbinary,kernel,iterations = 1)

def procesamiento():
    global imagenbinary, distanciaminimaesquinas, imagen
    global ancholinea, areaminimacontorno, formacuadrada
    global forma, dstcolor
    preprocesamiento()
    im2, contours, hierarchy = cv.findContours(imagenbinary,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    contour_sizes = [(cv.contourArea(contour), contour) for contour in contours]
    contorno_mayor = max(contour_sizes, key=lambda x: x[0])[1]
    #Obtengo el mayor contorno
    mascara = np.zeros(imagenbinary.shape, np.uint8)
    cv.drawContours(mascara, [contorno_mayor], -1, 255, -1)
    #Obtengo el rectangulo externo
    recta = cv.boundingRect(contorno_mayor)
    rect = cv.minAreaRect(contorno_mayor)
    box = cv.boxPoints(rect)
    box = np.int0(box)
    dst = imagenbinary
    diametro = max(recta[2],recta[3])
    if cv.contourArea(contorno_mayor) < diametro**2 * math.pi/4:
        formacuadrada = False
        forma = "Circulo"
        print("Circulo")
    else:
        forma = "Cuadrado"
        print("Cuadrado")
        formacuadrada = True
    resultados = [[False,0,0], [False,0,0], [False,0,0], [False,0,0]] #SI, SD, II, ID
    if formacuadrada:
        distanciax = 0
        distanciay = 0
        distanciamayorx = 100000
        distanciamayory = 100000
        puntoextremos = np.array([
            [0, 0],
            [ancho, 0],
            [ancho, alto],
            [0, alto]],
            dtype = "float32")
        esquinasordenadas = []
        #Codigo para encontrar las esquinas
        for i in puntoextremos:
            distancia = 0
            distanciamenor = 10000
            indice = 0
            for j in box:
                distancia = dist.euclidean((j[0],j[1]),(i[0],i[1]))
                if distancia < distanciamenor:
                    distanciamenor = distancia
                    indice = j
            esquinasordenadas.append(indice)

        anchoA = np.sqrt(((esquinasordenadas[3][0] - esquinasordenadas[2][0]) ** 2) + ((esquinasordenadas[3][1] - esquinasordenadas[2][1]) ** 2))
        anchoB = np.sqrt(((esquinasordenadas[1][0] - esquinasordenadas[0][0]) ** 2) + ((esquinasordenadas[1][1] - esquinasordenadas[0][1]) ** 2))
        anchoMaximo = max(int(anchoA), int(anchoB))

    	# compute the height of the new image, which will be the
    	# maximum distance between the top-right and bottom-right
    	# y-coordinates or the top-left and bottom-left y-coordinates
        alturaA = np.sqrt(((esquinasordenadas[1][0] - esquinasordenadas[2][0]) ** 2) + ((esquinasordenadas[1][1] - esquinasordenadas[2][1]) ** 2))
        alturaB = np.sqrt(((esquinasordenadas[0][0] - esquinasordenadas[3][0]) ** 2) + ((esquinasordenadas[0][1] - esquinasordenadas[3][1]) ** 2))
        alturaMaxima = max(int(alturaA), int(alturaB))
        recta = np.array([
                [esquinasordenadas[0][0], esquinasordenadas[0][1]],
                [esquinasordenadas[1][0], esquinasordenadas[1][1]],
                [esquinasordenadas[2][0], esquinasordenadas[2][1]],
                [esquinasordenadas[3][0], esquinasordenadas[3][1]]], dtype = "float32")

        arraydst = np.array([
        		[0, 0],
        		[anchoMaximo - 1, 0],
        		[anchoMaximo - 1, alturaMaxima - 1],
        		[0, alturaMaxima - 1]], dtype = "float32")
        M = cv.getPerspectiveTransform(recta, arraydst)
        '''print("Fuente")
        print(recta)
        print("Destino")
        print(arraydst)
        print("Matriz transformacion")
        print(M)'''
        dst = cv.warpPerspective(imagenbinary,M,(anchoMaximo, alturaMaxima))
        dstcolor = cv.warpPerspective(imagen,M,(anchoMaximo, alturaMaxima))

        dst = (255 - dst)
        global areafigura
        areafigura = dst.shape[0]*dst.shape[1]
        cv.line(dst,(0,0),(anchoMaximo,0),(0),ancholinea)
        cv.line(dst,(anchoMaximo,0),(anchoMaximo,alturaMaxima),(0),ancholinea)
        cv.line(dst,(anchoMaximo,alturaMaxima),(0,alturaMaxima),(0),ancholinea)
        cv.line(dst,(0,anchoMaximo),(0,0),(0),ancholinea)
        im2, contours, hierarchy = cv.findContours(dst,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
        for i in contours:
            area = cv.contourArea(i)
            if area/areafigura > areaminimacontorno:
                recta = cv.boundingRect(i)
                ratio = recta[2]/recta[3]
                if ratio > 0.9 and ratio < 1.1:
                    if area/areafigura > areaminimaaceptacion:
                        if int(recta[0] + recta[2]/2) < dst.shape[0]/3 and int(recta[1] + recta[1]/2) < dst.shape[1]/3:
                            resultados[0][0] = True
                            resultados[0][1] = int(recta[0] + recta[2]/2)
                            resultados[0][1] = int(recta[1] + recta[3]/2)
                        elif int(recta[0] + recta[2]/2) > 2*dst.shape[0]/3 and int(recta[1] + recta[1]/2) > 2*dst.shape[1]/3:
                            resultados[3][0] = True
                            resultados[0][1] = int(recta[0] + recta[2]/2)
                            resultados[0][1] = int(recta[1] + recta[3]/2)
                        elif int(recta[0] + recta[2]/2) < dst.shape[0]/3 and int(recta[1] + recta[1]/2) > 2*dst.shape[1]/3:
                            resultados[2][0] = True
                            resultados[0][1] = int(recta[0] + recta[2]/2)
                            resultados[0][1] = int(recta[1] + recta[3]/2)
                        elif int(recta[0] + recta[2]/2) > 2*dst.shape[0]/3 and int(recta[1] + recta[1]/2) < dst.shape[1]/3:
                            resultados[1][0] = True
                            resultados[0][1] = int(recta[0] + recta[2]/2)
                            resultados[0][1] = int(recta[1] + recta[3]/2)
                        cv.circle(dstcolor, (int(recta[0] + recta[2]/2), int(recta[1] + recta[3]/2)), 5, (0,255,0),-1)
        global errores
        errores = ""
        if resultados[0][0] == False:
            resultados[0][1] = int(1*dst.shape[0]/5)
            resultados[0][2] = int(1*dst.shape[0]/5)
            cv.circle(dstcolor, (int(1*dst.shape[0]/5), int(1*dst.shape[0]/5)), 5, (0,0,255),-1)
            errores += "Agujero mal hecho en la parte superior izquierda (" + str(resultados[0][1]) + "," + str(resultados[0][2]) + ")\n"
        elif resultados[1][0] == False:
            resultados[1][1] = int(4*dst.shape[0]/5)
            resultados[1][2] = int(1*dst.shape[0]/5)
            errores += "Agujero mal hecho en la parte superior derecha (" + str(resultados[1][1]) + "," + str(resultados[1][2]) + ")\n"
            cv.circle(dstcolor, (int(4*dst.shape[0]/5), int(1*dst.shape[0]/5)), 5, (0,0,255),-1)
        elif resultados[2][0] == False:
            resultados[2][1] = int(1*dst.shape[0]/5)
            resultados[2][2] = int(4*dst.shape[0]/5)
            errores += "Agujero mal hecho en la parte inferior izquierda (" + str(resultados[2][1]) + "," + str(resultados[2][2]) + ")\n"
            cv.circle(dstcolor, (int(1*dst.shape[0]/5), int(4*dst.shape[0]/5)), 5, (0,0,255),-1)
        elif resultados[3][0] == False:
            resultados[3][1] = int(4*dst.shape[0]/5)
            resultados[3][2] = int(4*dst.shape[0]/5)
            errores += "Agujero mal hecho en la parte inferior derecha (" + str(resultados[3][1]) + "," + str(resultados[3][2]) + ")\n"
            cv.circle(dstcolor, (int(4*dst.shape[0]/5), int(4*dst.shape[0]/5)), 5, (0,0,255),-1)
        if platform.machine() == 'x86_64':
            cv.imshow("ROI",dst)
            cv.imshow("Procesada",imagenbinary)
            cv.imshow("DSTColor",dstcolor)
    else:
        errores = ""
        dst = imagenbinary[int(recta[1]):int(recta[1]+recta[3]), int(recta[0]):int(recta[0]+recta[2])]
        dstcolor = imagen[int(recta[1]):int(recta[1]+recta[3]), int(recta[0]):int(recta[0]+recta[2])]
        dst = (255 - dst) #Invierto la máscara para diferenciar las
        centrox = dst.shape[0]/2
        centroy = dst.shape[1]/2
        im2, contours, hierarchy = cv.findContours(dst,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)

        resultados = []
        for i in contours:
            M = cv.moments(i)
            if M['m00'] == 0:
                M['m00'] = 0.0001
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            distancia = dist.euclidean((centrox,centroy),(cx,cy))
            if distancia < diametro/2 and distancia/diametro > 0.10:
                if 2*distancia/diametro < 0.7:
                    cv.circle(dstcolor, (int(cx), int(cy)), 5, (0,255,0),-1)
                    vector = [True, cx, cy]
                    resultados.append(vector)
                else:
                    cv.circle(dstcolor, (int(cx), int(cy)), 5, (0,0,255),-1)
                    vector = [False, cx, cy]
                    resultados.append(vector)
        for i in resultados:
            if i[0] == False:
                print("Cuadrado desalineado en ("+ str(i[1]) +"," + str(i[2]) +  ")")
                errores += "Cuadrado desalineado en ("+ str(i[1]) +"," + str(i[2]) +  ")"
        if platform.machine() == 'x86_64':
            cv.imshow("ROI", dst)
            cv.imshow("DSTColor",dstcolor)
            cv.imshow("Procesada",imagenbinary)
    cv.drawContours(imagen,[box],0,(0,0,255),2)
    puntocolor = dstcolor[int(dstcolor.shape[0]/2 + 0.18*dstcolor.shape[0])][int(dstcolor.shape[1]/2)]/4 + \
        dstcolor[int(dstcolor.shape[0]/2 - 0.18*dstcolor.shape[0])][int(dstcolor.shape[1]/2)]/4 + \
        dstcolor[int(dstcolor.shape[0]/2)][int(dstcolor.shape[1]/2 + 0.18*dstcolor.shape[1])]/4 + \
        dstcolor[int(dstcolor.shape[0]/2)][int(dstcolor.shape[1]/2 - 0.18*dstcolor.shape[1])]/4
    matrizcolor = np.array([[puntocolor]],dtype = "uint8")
    if platform.machine() == 'x86_64':
        cv.imshow("DSTColor",dstcolor)
        cv.imshow("Matriz color", matrizcolor)
    matrizcolor = cv.cvtColor(matrizcolor,cv.COLOR_BGR2HSV)
    global valorsgris, valorvblanco, valorhverdeminimo, valorhverdemaximo, color
    print(matrizcolor)
    if matrizcolor[0][0][1] < valorsgris:
        color = "Gris"
        print("Gris")
    elif matrizcolor[0][0][0] >= valorhverdeminimo and matrizcolor[0][0][0] <= valorhverdemaximo:
        print("Verde")
        color = "Verde"
    elif matrizcolor[0][0][0] >= valorhamarillominimo and matrizcolor[0][0][0] <= valorhamarillomaximo:
        print("Amarillo")
        color = "Amarillo"
    else:
        color = "Falta de implementar en codigo"
        print("Color no aceptado por la banda")
    # Convert captured image to JPG
    retval, buffer = cv.imencode('.png', dstcolor)

    # Convert to base64 encoding and show start of data
    dstcolor_base64 = base64.b64encode(buffer).decode("utf-8")
    if len(resultados) > 0:
        final = True
        for i in resultados:
            final = final and i[0]
        return final, forma, color, dstcolor_base64, errores
    else:
        return False, forma, color, dstcolor_base64, errores

def clasificacion():
    pass
