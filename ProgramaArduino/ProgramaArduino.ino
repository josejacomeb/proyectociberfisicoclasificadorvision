
#define DEBUG(a) Serial.println(a);

//GUARDAR ESTADO ENTRADAS
int sd0 = 0;
int sd1 = 0;
int sd2 = 0;
int sd3 = 0;
int sd4 = 0;

int sd6 = 0;
int sa7 = 0;
int sa8 = 0;
int sa9 = 0;
int sa10 = 0;
int sa11 = 0;
int sa12 = 0;
int sic = 0; //Sensor infrarrojo camara
int pwmbanda = 200;
void controlbanda(int pwm);
bool inicioproceso = false;
bool magnetico = false;
bool visionrealizada = false;
char clasificador = '0';
//

bool esd0 = false;
bool esd1 = false;
bool esd2 = false;
bool esd3 = false;
bool esd4 = false;
bool esa7 = false;
bool esa8 = false;
bool esa9 = false;
bool esa10 = false;
bool unavez = false;
bool unavezinicio = false;



bool anterioresd0 = false;
bool anterioresd1 = false;
bool anterioresd2 = false;
bool anterioresd3 = false;
bool anterioresd4 = false;
bool anterioresa7 = false;
bool anterioresa8 = false;
bool anterioresa9 = false;
bool anterioresa10 = false;

//Bool estados paro y fin
bool paroweb = true;
bool inicioweb = true;
int tiempodelaycilindros = 500;


void setup() {
  Serial.begin(9600);

  //Entradas Digitales
  pinMode(  I0_0, INPUT); //sd0 on/off
  pinMode(  I0_1, INPUT); //sd1 paro de emergencia
  pinMode(  I0_2, INPUT); //sd2 inductivo.
  pinMode(  I0_3, INPUT); //sd3 magnetico 1 h
  pinMode(  I0_4, INPUT); //sd4 magnetico 2


  //Entradas Analogas
  pinMode(  I0_8, INPUT); //sa7 sensor infra pre 1
  pinMode(  I0_7, INPUT); //sa8 sensor infra pre 2
  pinMode(  I0_9, INPUT); //sa9 sensor infra inicio
  pinMode(  I0_10, INPUT); //sa10 sensor infra bajo camara
  //Salidas Digitales
  pinMode(  Q0_0, OUTPUT); // Electro Val 1
  pinMode(  Q0_1, OUTPUT); // Electro Val 2
  pinMode(  Q0_2, OUTPUT); // Relay Piloto Verde
  pinMode(  Q0_3, OUTPUT); // Relay Piloto Rojo
  pinMode(  Q0_4, OUTPUT); // in1
  pinMode(  Q0_5, OUTPUT); // in2
  pinMode(  Q0_6, OUTPUT); // pwm
  digitalWrite( Q0_0, LOW);
  digitalWrite( Q0_1, LOW);
  sd1 = digitalRead(I0_1);
}


void loop() {

  //GUARDAR ESTADO ENTRADAS
  sd0 = digitalRead(I0_0);
  sd1 = digitalRead(I0_1);
  sd2 = digitalRead(I0_2);
  sd3 = digitalRead(I0_3);
  sd4 = digitalRead(I0_4);

  sd6 = digitalRead(I0_6);
  sa7 = analogRead(I0_8);
  sa8 = analogRead(I0_7);
  sa9 = analogRead(I0_9);
  sa10 = analogRead(I0_10);
  sa11 = analogRead(I0_11);
  sa12 = analogRead(I0_12);
  sic = analogRead(I0_10);
  // inductivo

  if (sd2 == HIGH) {

    esd2 = true;
  }
  else {
    esd2 = false;
  }
  if (esd2 != anterioresd2) {

    if (esd2 == true) {
      Serial.print('c');
    }
    else {
      Serial.print('e');
    }

  }
  anterioresd2 = esd2;

  //sensor inicio
  if (sa9 < 255) {

    esa9 = true;
  }
  else {
    esa9 = false;
  }
  if (esa9 != anterioresa9) {

    if (esa9 == true) {
      Serial.print('f');
    }
    else {
      Serial.print('g');
    }

  }
  anterioresa9 = esa9;



  //sesor precencia camara
  if (sa8 < 255) {

    esa8 = true;
  }
  else {
    esa8 = false;
  }
  if (esa8 != anterioresa8) {

    if (esa8 == true) {
      Serial.print('h');
    }
    else {
      Serial.print('i');
    }
  }
  anterioresa8 = esa8;

  //sensor pre 1
  if (sa7 < 255) {

    esa7 = true;
  }
  else {
    esa7 = false;
  }
  if (esa7 != anterioresa7) {

    if (esa7 == true) {
      Serial.print('j');
    }
    else {
      Serial.print('k');
    }
  }
  anterioresa7 = esa7;
  //sensor pre 2
  if (sa10 < 255) {

    esa10 = true;
  }
  else {
    esa10 = false;
  }
  if (esa10 != anterioresa10) {

    if (esa10 == true) {
      Serial.print('l');
    }
    else {
      Serial.print('m');
    }
  }
  anterioresa10 = esa10;

  if (Serial.available()) {
    char c = Serial.read();
    if (c == 'n')
      digitalWrite(Q0_0, HIGH);
    else if  (c == 'o')
      digitalWrite(Q0_0, LOW);
    else if  (c == 'p')
      digitalWrite(Q0_1, HIGH);
    else if  (c == 'q')
      digitalWrite(Q0_1, LOW);
    else if  (c == '3') {
      pwmbanda = 0;
      if (inicioproceso) controlbanda(pwmbanda);
    }
    else if  (c == '4') {
      pwmbanda = 51;
      if (inicioproceso) controlbanda(pwmbanda);
    }
    else if  (c == '5') {
      pwmbanda = 102;
      if (inicioproceso) controlbanda(pwmbanda);
    }
    else if  (c == '6') {
      pwmbanda = 153;
      if (inicioproceso) controlbanda(pwmbanda);
    }
    else if  (c == '7') {
      pwmbanda = 204;
      if (inicioproceso) controlbanda(pwmbanda);

    }
    else if  (c == '8') {
      pwmbanda = 255;
      if (inicioproceso) controlbanda(pwmbanda);
    }
    else if (c == '9')
      inicioweb = true;
    else if (c == '0') {
      inicioweb = false;
      paroweb = false;
    }
    else if (c == '.') {
      inicioweb = false;
      paroweb = true;
    }

  }
  if (sd1 == HIGH && paroweb) { //Paro de Emergencia
    digitalWrite(Q0_3, LOW);
    if (unavez) Serial.print("w");
    unavez = false;
    if (sd0 == HIGH && inicioweb) { //Selector de Inicio
      digitalWrite(Q0_2, HIGH);
      if (!unavezinicio) {
        Serial.print("z"); //t paro u fallo piston 1 v fallo piston 2
      }
      unavezinicio = true;
      if (!inicioproceso && sa9 < 255) {
        delay(750);
        controlbanda(pwmbanda);
        inicioproceso = true;
        magnetico = false;
        visionrealizada = false;
      }

      if (inicioproceso && sd2) { //Sensar el magnetico
        magnetico = true;
      }
      if (inicioproceso && sa10 < 255 && !visionrealizada) {
        controlbanda(0);
        delay(1000);
        if (magnetico) Serial.print(1);
        else Serial.print(2);

        while (!Serial.available()) {
          if (sd1 == LOW) break;
        } //Espero hasta que haya datos
        if (Serial.available()) {
          clasificador = Serial.read();
        }
        controlbanda(pwmbanda);
        visionrealizada = true;
      }
      if (visionrealizada & clasificador == 'a') { // Si esta bien la pieza, pasa a la bandeja 2 de acuerdo a la configuracion
        if (inicioproceso && sa7 < 255) {
          controlbanda(0);
          delay(200);
          digitalWrite( Q0_0, HIGH);
          Serial.print('n');
          delay(tiempodelaycilindros);
          sd3 = digitalRead(I0_3);
          if (sd3 == LOW) { // no se extendio el cilindro
            Serial.print("u");
            while (sd3 == LOW) {
              sd3 = digitalRead(I0_3);
            }
          }
          digitalWrite( Q0_0, LOW);
          delay(tiempodelaycilindros);
          sd3 = digitalRead(I0_3);
          if (sd3 == HIGH) { // no se extendio el cilindro
            Serial.print("x");
            while (sd3 == HIGH) {
              sd3 = digitalRead(I0_3);
            }
          }
          Serial.print('o');
          Serial.print("w");
          inicioproceso = false;
          visionrealizada = false;
          clasificador == '0';
        }
      }
      else if (visionrealizada && clasificador == 'b') { //Si esta mal la pieza passa recto
        controlbanda(pwmbanda);
        delay(5000);
        controlbanda(0);
        inicioproceso = false;
        visionrealizada = false;
        clasificador == '0';
      }
      else if (visionrealizada && clasificador == 'c') {// Si esta bien la pieza, pasa a la bandeja 2 de acuerdo a la configuracion
        if (inicioproceso && sa8 < 255) {
          controlbanda(0);
          delay(200);
          digitalWrite( Q0_1, HIGH);
          Serial.print('p');
          delay(tiempodelaycilindros);
          sd4 = digitalRead(I0_4);
          if (sd4 == LOW) { // no se extendio el cilindro
            Serial.print("v");
            while (sd4 == LOW) {
              sd4 = digitalRead(I0_4);
            }
          }
          digitalWrite( Q0_1, LOW);
          delay(tiempodelaycilindros);
          sd4 = digitalRead(I0_4);
          if (sd4 == HIGH) { // no se extendio el cilindro
            Serial.print("y");
            while (sd4 == HIGH) {
              sd4 = digitalRead(I0_4);
            }
          }
          Serial.print('q');
          Serial.print('w');
          inicioproceso = false;
          visionrealizada = false;
          clasificador = '0';
        }
      }
    } else {
      inicioproceso = false;
      if (unavezinicio) {
        Serial.print("w");
      }
      unavezinicio = false;
      visionrealizada = false;
      clasificador = '0';
      digitalWrite(Q0_2, LOW);
      digitalWrite(Q0_3, LOW);
      controlbanda(0);
    }
  } else {
    if (!unavez) {
      Serial.print("t");
    }
    unavez = true;
    inicioproceso = false;
    visionrealizada = false;
    clasificador = '0';
    digitalWrite(Q0_2, LOW);
    digitalWrite(Q0_3, HIGH);
    controlbanda(0);
  }
}
void controlbanda(int pwm) {
  if (pwm == 0) {
    digitalWrite(Q0_4, LOW);
    digitalWrite(Q0_5, LOW);
    analogWrite(Q0_6, pwm);
  }
  else if (pwm > 0) {
    digitalWrite(Q0_4, HIGH);
    digitalWrite(Q0_5, LOW);
    analogWrite(Q0_6, pwm);
  }
  else if (pwm < 0) {
    digitalWrite(Q0_4, LOW);
    digitalWrite(Q0_5, HIGH);
    analogWrite(Q0_6, -pwm);
  }
}
