#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Antes de correr el programa, asegurse que esté instalado pip2 install pyserial
__name__ = "comserial"
import serial            #Importa la libreria serial
import time   #Delay como en Arduino
ser = serial.Serial() #Declaro un objeto serial
comunicacionactivada = False
def iniciarserial(puerto, baudios=9600):
    """
        Clase iniciarserial, inicializa el puerto serial, devuelve true si hay conexion y
        false si no es posible establecer conexion
    """
    global ser
    print('*****INICIO DEL PROGRAMA*****')
    global comunicacionactivada
    ser.baudrate = baudios     #Especifica la velocidad de transferencia
    ser.port = puerto #Abre el puerto serial para Arduino, siempre termina en ACM0 o ACM1
    global comunicacionactivada
    try:
        ser.open()
    except Exception as e:
        print("No se puede abrir el serial")
        comunicacionactivada =False
        return False
    print('Puerto serial abierto')
    time.sleep(1)
    ser.dtr = True # Desactiva DTR, para iniciar el puerto serial, es decir resetear el microcontrolador para iniciar comunicacion.
    time.sleep(1)    #Espera un tiempo
    comunicacionactivada = True
    print("Comunicacion activada")
    return True

def enviardatos(dato):
    """
        Funcion por si se quiere enviar datos
    """
    global ser
    datostr = dato.encode('utf-8')
    ser.write(datostr)
    print("Dato enviado: ", datostr)

def recibirdatos():
    """
        Función para recibir datos, de un buffer de un solo caracter solamente
    """
    global ser
    datosbuffer = ser.in_waiting
    w = b'b'
    if datosbuffer:
        w = ser.read(1)
        print("Dato recibido: ", w)
    return w

def estadocomunicacion():
    """
        Devuelve si la comunicacion esta activada o no
    """
    global comunicacionactivada
    return comunicacionactivada

def terminarserial(): #Funcion para terminar el serial
    ser.close()
