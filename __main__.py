#!/usr/bin/python3
# -*- coding: utf-8 -*-
import comserial
import vision
import socketio
'''
    Importante, primero compilar la libreria de opencv desde la pagina oficial
    incluir BUILD python3 y python_bind_generators

'''
if vision.platform.machine() == 'x86_64':
    cap = vision.cv.VideoCapture(1)
else:
    cap = vision.cv.VideoCapture(0)

if __name__ == '__main__':
    global imagen
    estado = False
    metal = False
    comserial.iniciarserial("/dev/ttyACM0")
    socketio.inicializar("localhost",3000)
    diccionario = {"cadena64":"", "forma":"","color":"", "errores":""}
    valores = {"nombre":"estado", "estado": 0}; #Reiniciar el Arduino y variables
    socketio.enviarvalores("estado",valores)
    while(True):
        ret, vision.imagen = cap.read()
        if vision.platform.machine() == 'x86_64':
            vision.cv.imshow("Original",vision.imagen)
        c = b'a'
        c = comserial.recibirdatos()
        if c == b'n':
            valores = {"nombre":"Piston1","valor":True}
            socketio.enviarvalores("valores", valores)
        elif c == b'o':
            valores = {"nombre":"Piston1","valor":False}
            socketio.enviarvalores("valores", valores)
        elif c == b'p':
            valores = {"nombre":"Piston2","valor":True}
            socketio.enviarvalores("valores", valores)
        elif c == b'q':
            valores = {"nombre":"Piston2","valor":False}
            socketio.enviarvalores("valores", valores)
        elif c == b'f':
            valores = {"nombre":"sensorPresencia1","valor":True}
            socketio.enviarvalores("valores", valores)
        elif c == b'g':
            valores = {"nombre":"sensorPresencia1","valor":False}
            socketio.enviarvalores("valores", valores)
        elif c == b'h':
            valores = {"nombre":"sensorPresencia2","valor":True}
            socketio.enviarvalores("valores", valores)
        elif c == b'i':
            valores = {"nombre":"sensorPresencia2","valor":False}
            socketio.enviarvalores("valores", valores)
        elif c == b'j':
            valores = {"nombre":"sensorPresencia3","valor":True}
            socketio.enviarvalores("valores", valores)
        elif c == b'k':
            valores = {"nombre":"sensorPresencia3","valor":False}
            socketio.enviarvalores("valores", valores)
        elif c == b'l':
            valores = {"nombre":"sensorPresencia4","valor":True}
            socketio.enviarvalores("valores", valores)
        elif c == b'm':
            valores = {"nombre":"sensorPresencia4","valor":False}
            socketio.enviarvalores("valores", valores)
        elif c == b'c':
            valores = {"nombre":"sensorInductivo","valor":True}
            metal = True
            socketio.enviarvalores("valores", valores)
        elif c == b'e':
            valores = {"nombre":"sensorInductivo","valor":False}
            socketio.enviarvalores("valores", valores)
        elif c == b't':
            valores = {"nombre":"Emergencia","valor":"Activado el paro de emergencia"}
            socketio.enviarvalores("valores", valores)
            valores = {"nombre":"estado", "estado": -1};
            socketio.enviarvalores("estado",valores)
        elif c == b'u':
            valores = {"nombre":"Emergencia","valor":"No se ha extendido el Piston 1"}
            socketio.enviarvalores("valores", valores)
        elif c == b'x':
            valores = {"nombre":"Emergencia","valor":"No se ha retraido el Piston 1"}
            socketio.enviarvalores("valores", valores)
        elif c == b'v':
            valores = {"nombre":"Emergencia","valor":"No se ha extendido el Piston 2"}
            socketio.enviarvalores("valores", valores)
        elif c == b'y':
            valores = {"nombre":"Emergencia","valor":"No se ha retraido el Piston 2"}
            socketio.enviarvalores("valores", valores)
        elif c == b'w':
            valores = {"nombre":"liberar","valor":"Liberada las alarmas"}
            socketio.enviarvalores("valores", valores)
            valores = {"nombre":"estado", "estado": 0};
            socketio.enviarvalores("estado",valores)
        elif c == b'z':
            valores = {"nombre":"estado", "estado": 1};
            socketio.enviarvalores("estado",valores)
        elif c == b'.':
            valores = {"nombre":"estado", "estado": 0};
            socketio.enviarvalores("estado",valores)
        elif c == b'1' or c == b'2' : # 1 Metal, 2 no metal
            if c == b'2':
                metal = False
            bien, forma, color, base64, errores = vision.procesamiento()
            diccionario["forma"] = forma
            diccionario["color"] = color
            diccionario["cadena64"] = base64
            diccionario["errores"] = errores
            if metal:
                diccionario["composicion"] = "Metal"
            else:
                diccionario["composicion"] = "No metal"
            socketio.enviarvalores("imagen",diccionario)
            if bien:
                if comserial.comunicacionactivada:
                    print("**********************")
                    print("Bandeja 1: ", socketio.bandeja1)
                    print("Bandeja 2: ", socketio.bandeja2)
                    if socketio.modo == "cmnm":
                        print("Bandeja 1: ",socketio.bandeja1)
                        print("Metal: ", metal)
                        if metal and socketio.bandeja1=="Metal":
                            print("Metal y bandeja1 metal")
                            comserial.enviardatos('a')
                            metal = False
                        elif not metal and socketio.bandeja1=="No metal":
                            print("Bandeja1 no metal")
                            comserial.enviardatos('a')
                        else:
                            print("Otra bandeja")
                            comserial.enviardatos('c')
                    elif socketio.modo == "cf":
                        print("Bandeja 1: ", socketio.bandeja1)
                        print("Forma: ", forma)
                        if socketio.bandeja1 == forma:
                            comserial.enviardatos('a')
                        else:
                            comserial.enviardatos('c')
                    elif socketio.modo == "cc":
                        print("Bandeja 1: ", socketio.bandeja1)
                        print("Color: ", color)
                        if socketio.bandeja1 == "Otros":
                            if socketio.bandeja2 == color:
                                comserial.enviardatos('c')
                            else:
                                comserial.enviardatos('a')
                        else:
                            if socketio.bandeja1 == color:
                                comserial.enviardatos('a')
                            else:
                                comserial.enviardatos('c')
            else:
                if comserial.comunicacionactivada:
                    comserial.enviardatos('b')
                pass
        if vision.cv.waitKey(30) & 0xFF == ord('q'):
            break

    vision.cv.destroyAllWindows()
