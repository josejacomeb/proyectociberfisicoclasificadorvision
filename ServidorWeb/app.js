var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var users = require('./routes/users');
var fs = require('fs'); //require filesystem module
var numeroconectados = 0;
var numeroimagenes = 0;
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var fechaimagen = "";
var numeroimagenes = 0;
var direccioncorreo = "djavier1492@gmail.com";
var imagenes = {//Donde guardar las imagenes
    nombre: "",
    fecha: new Date(),
    forma: "",
    color: "",
    errores: ""
};
var pistones = {//Donde guardar las imagenes
    ciclopiston1: 0,
    ciclopiston2: 0,
    ciclosmaximospiston1: 5000,
    ciclosmaximospiston2: 5000
};
var datodispositivo = {
    nombre: "estado",
    estado: 0
};

var rutaimagenprocesada = "";

var datoimagenprocesada = {};
var datomodo = {nombre: "modo", modo: "cmnm", bandeja1: "Metal", bandeja2: "No metal"};
var datovalores = {nombre: "Piston1", valor: false};
//INICIO PARTE MANUAL

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(require('less-middleware')({src: path.join(__dirname, 'public')}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forwarding to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'labmct1@gmail.com',
        pass: 'espe2018mct'
    }
});

//Cargo el JSON si este existe
fs.readFile(__dirname + '/public/images/imagenes.json', 'utf8', function (err, data) {
    if (!err) {
        obj = data.split(/\r?\n/);
        numeroimagenes = obj.length - 2;
        var datoJSON = JSON.parse(obj[numeroimagenes]);
        rutaimagenprocesada = datoJSON.nombre;
        datoimagenprocesada.nombre = rutaimagenprocesada;
        datoimagenprocesada.forma = datoJSON.forma;
        datoimagenprocesada.color = datoJSON.color;
        datoimagenprocesada.errores = datoJSON.errores;
        datoimagenprocesada.composicion = datoJSON.composicion;
    } else {
        numeroimagenes = 0;
        totalimagenes = [];
        rutaimagenprocesada = "images/mct.jpg";
        datoimagenprocesada.nombre = rutaimagenprocesada;
        datoimagenprocesada.forma = "-";
        datoimagenprocesada.color = "-";
        datoimagenprocesada.errores = "-";
        datoimagenprocesada.composicion = "-";

    }
});
//Cargo el JSON si este existe
fs.readFile(__dirname + '/public/ciclosuso.json', 'utf8', function (err, data) {
    if (!err) {
        obj = JSON.parse(data);
        pistones.ciclopiston1 = obj.ciclopiston1;
        pistones.ciclopiston2 = obj.ciclopiston2;
        pistones.ciclosmaximospiston1 = obj.ciclosmaximospiston1;
        pistones.ciclosmaximospiston2 = obj.ciclosmaximospiston2;
    } else {
        pistones.ciclopiston1 = 0;
        pistones.ciclopiston2 = 0;
        pistones.ciclosmaximospiston1 = 5000;
        pistones.ciclosmaximospiston2 = 5000;
    }
});
io.on('connection', function (socket) {
    numeroconectados += 1;
    console.log("<<Usuario conectado>> Usuarios en linea: " + numeroconectados);
    socket.emit("valores", datovalores);
    socket.emit("imagenprocesada", datoimagenprocesada);
    socket.emit("modo", datomodo);
    socket.emit('estado', datodispositivo);
    socket.on('disconnect', function () {
        numeroconectados -= 1;
        console.log('<<Usuario desconectado>> Usuarios en linea: ' + numeroconectados);
    });
    socket.on("valores", function (data) {
        console.log(data);
        datovalores = data;
        if (data.nombre === "Piston1") {
            if (Number.isInteger(data.valor)) {
                pistones.ciclopiston1 = data.valor;
                var ciclostr = JSON.stringify(pistones);
                fs.writeFile(__dirname + "/public/ciclosuso.json", ciclostr, 'utf8', function (err) {
                    console.log("JSON Ciclo de usogenerado correctamente");
                });
            } else {
                if (data.valor === true) {
                    pistones.ciclopiston1 += 1;
                    var ciclostr = JSON.stringify(pistones);
                    fs.writeFile(__dirname + "/public/ciclosuso.json", ciclostr, 'utf8', function (err) {
                        console.log("JSON Ciclo de usogenerado correctamente");
                    });
                }
            }
        } else if (data.nombre === "Piston2") {
            if (Number.isInteger(data.valor)) {
                pistones.ciclopiston2 = data.valor;
                var ciclostr = JSON.stringify(pistones);
                fs.writeFile(__dirname + "/public/ciclosuso.json", ciclostr, 'utf8', function (err) {
                    console.log("JSON Ciclo de usogenerado correctamente");
                });
            } else {
                if (data.valor === true) {
                    pistones.ciclopiston2 += 1;
                    var ciclostr = JSON.stringify(pistones);
                    fs.writeFile(__dirname + "/public/ciclosuso.json", ciclostr, 'utf8', function (err) {
                        console.log("JSON ciclo de uso generado correctamente");
                    });
                }
            }
        } else if (data.nombre === "Emergencia") {
            var fecha = new Date();
            var fechactual = fecha.toUTCString();
            var salidacsv = fechactual + ";Emergencia;" + data.valor + ";\n";
            var mailOptions = {
                from: 'josejacomeb@gmail.com',
                to: direccioncorreo,
                subject: 'Evento de emergencia detectado en el sistema',
                text: "Evento registrado a las " + new Date().toUTCString() + "\n" + data.valor
            };
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email enviado: ' + info.response);
                }
            });
            fs.appendFile(__dirname + "/public/emergencia.csv", salidacsv, 'utf8', function (err) {
                console.log("Archivo emergencias generado correctamente");
            });
        } else if (data.nombre === "Alerta") {
            var fecha = new Date();
            var fechactual = fecha.toUTCString();
            var salidacsv = fechactual + ";Alarma;" + data.valor + ";\n";
            var mailOptions = {
                from: 'josejacomeb@gmail.com',
                to: direccioncorreo,
                subject: 'Evento de alarma detectado en el sistema',
                text: "Evento registrado a las " + new Date().toUTCString() + "\n" + data.valor
            };
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email enviado: ' + info.response);
                }
            });
            fs.appendFile(__dirname + "/public/alarmas.csv", salidacsv, 'utf8', function (err) {
                console.log("Archivo alarmas generado correctamente");
            });
        } else if (data.nombre === "Correo") {
            direccioncorreo = data.valor;
            console.log("Direccion Correo");
            console.log(direccioncorreo);
        }
        socket.broadcast.emit('valores', data); //Envia a todos los clientes
    });
    socket.on("modo", function (data) {
        datomodo = data;
        socket.broadcast.emit('modo', data); //Envia a todos los clientes
    });
    socket.on("ciclouso", function (data) {
        pistones = data;
        var ciclostr = JSON.stringify(pistones);
        fs.writeFile(__dirname + "/public/ciclosuso.json", ciclostr, 'utf8', function (err) {
            console.log("JSON ciclo de uso generado correctamente");
        });
        socket.broadcast.emit('ciclouso', data); //Envia a todos los clientes4
    });
    socket.on("estado", function (data) {
        datodispositivo = data;
        socket.broadcast.emit('estado', datodispositivo);
    });
    socket.on('imagen', function (data) {

        var fecha = new Date();
        var fechactual = fecha.toISOString();
        rutaimagenprocesada = "/images/imagenesprocesadas/img" + numeroimagenes + ".png";
        imagenes.nombre = rutaimagenprocesada;
        imagenes.fecha = fechactual;
        imagenes.errores = data.errores;
        imagenes.forma = data.forma;
        imagenes.color = data.color;
        imagenes.composicion = data.composicion;
        var variableimagenes = JSON.stringify(imagenes);
        fs.writeFile(__dirname + "/public/images/imagenesprocesadas/img" + numeroimagenes + ".png", data.cadena64, 'base64', function (err) {
            console.log("Imagen guardada en:" + __dirname + "/public/images/imagenesprocesadas/img" + numeroimagenes + ".png");
        });
        variableimagenes += "\n";
        fs.appendFile(__dirname + "/public/images/imagenes.json", variableimagenes, 'utf8', function (err) {
            console.log("JSON generado correctamente");
        });
        socket.broadcast.emit('imagenprocesada', imagenes); //Envia a todos los clientes
        datoimagenprocesada = imagenes;
        imagenes = {};
        //Guardar en base de datos
        numeroimagenes += 1;
    });
});
module.exports = {app: app, server: server};

