var socket = io(); // CREA UNA VARIABLE PARA ENVIAR AL CLIENTE DEPEDNIEDO DE LA VARIABLE EN HTML
var myObj = {};
var x = 0;
var opcion = document.getElementById("opcion");
var historicopiezas = document.getElementById("historicopiezas");
var xmlhttp = new XMLHttpRequest();
var body = document.getElementById("divtabla");
var options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'};
socket.on('nueva_imagen', function (data) {
    console.log("Recargando todo!");
    dibujar();
});
// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages': ['corechart']});
// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function estadisticoForma() {
    body.innerHTML = "";
    var buenascirculo = 0, buenascuadrado = 0;
    var malascirculo = 0, malascuadrado = 0;
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            myObj = this.responseText.split(/\r?\n/);
            for (var x = 0; x < myObj.length - 1; x++) {
                var objetoJSON = JSON.parse(myObj[x]);
                if (objetoJSON.forma === "Circulo") {
                    if (objetoJSON.errores === "") {
                        buenascirculo += 1;
                    } else {
                        malascirculo += 1;
                    }
                } else {
                    if (objetoJSON.errores === "") {
                        buenascuadrado += 1;
                    } else {
                        malascuadrado += 1;
                    }
                }

            }
            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Buenas');
            data.addColumn('number', 'Malas');
            data.addRows([
                ['Circulo', buenascirculo, malascirculo],
                ['Cuadrado', buenascuadrado, malascuadrado]
            ]);
            // Set chart options
            var options = {'title': 'Piezas buenas y malas por forma',
                'width': window.outerWidth,
                'height': window.outerHeight};
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.BarChart(document.getElementById('divtabla'));
            chart.draw(data, options);
        }

    };
    xmlhttp.open("GET", "/images/imagenes.json", true);
    xmlhttp.send();
}
function estadisticoComposicion() {
    body.innerHTML = "";
    var buenasmetal = 0, buenasnometal = 0;
    var malasmetal = 0, malasnometal = 0;
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            myObj = this.responseText.split(/\r?\n/);
            for (var x = 0; x < myObj.length - 1; x++) {
                var objetoJSON = JSON.parse(myObj[x]);
                if (objetoJSON.composicion === "Metal") {
                    if (objetoJSON.errores === "") {
                        buenasmetal += 1;
                    } else {
                        malasmetal += 1;
                    }
                } else {
                    if (objetoJSON.errores === "") {
                        buenasnometal += 1;
                    } else {
                        malasnometal += 1;
                    }
                }

            }
            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Buenas');
            data.addColumn('number', 'Malas');
            data.addRows([
                ['Metal', buenasmetal, malasmetal],
                ['No Metal', buenasnometal, malasnometal]
            ]);
            // Set chart options
            var options = {'title': 'Piezas buenas y malas por material',
                'width': window.outerWidth,
                'height': window.outerHeight};
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.BarChart(document.getElementById('divtabla'));
            chart.draw(data, options);
        }

    };
    xmlhttp.open("GET", "/images/imagenes.json", true);
    xmlhttp.send();
}
function estadisticoColor() {
    body.innerHTML = "";
    var buenasamarillo = 0, buenasgris = 0, buenasverde = 0;
    var malasamarillo = 0, malasgris = 0, malasverde = 0;
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            myObj = this.responseText.split(/\r?\n/);
            for (var x = 0; x < myObj.length - 1; x++) {
                var objetoJSON = JSON.parse(myObj[x]);
                if (objetoJSON.color === "Verde") {
                    if (objetoJSON.errores === "") {
                        buenasverde += 1;
                    } else {
                        malasverde += 1;
                    }
                } else if (objetoJSON.color === "Gris") {
                    if (objetoJSON.errores === "") {
                        buenasgris += 1;
                    } else {
                        malasgris += 1;
                    }
                } else {
                    if (objetoJSON.errores === "") {
                        buenasamarillo += 1;
                    } else {
                        malasamarillo += 1;
                    }
                }

            }
            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Buenas');
            data.addColumn('number', 'Malas');
            data.addRows([
                ['Verde', buenasverde, malasverde],
                ['Amarillo', buenasamarillo, malasamarillo],
                ['Gris', buenasgris, malasgris]
            ]);
            // Set chart options
            var options = {'title': 'Piezas buenas y malas por Color',
                'width': window.outerWidth,
                'height': window.outerHeight};
            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.BarChart(document.getElementById('divtabla'));
            chart.draw(data, options);
        }

    };
    xmlhttp.open("GET", "/images/imagenes.json", true);
    xmlhttp.send();
}



function dibujar() {

    var body = document.getElementById("divtabla");
    body.innerHTML = "";
    var tabla = document.createElement("table");
    tabla.setAttribute("class", "w3-table w3-striped w3-bordered");
    tabla.setAttribute("id", "tablahistorial");
    var tblBody = document.createElement("tbody");
    var xmlhttp = new XMLHttpRequest();
    var tblHeader = document.createElement("thead");
    var hilera = document.createElement("tr");
    var textoCelda = document.createTextNode("Nº");
    var celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    var textoCelda = document.createTextNode("Imagen");
    celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    var textoCelda = document.createTextNode("Descripcion");
    celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    var textoCelda = document.createTextNode("Fecha");
    celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    tblHeader.appendChild(hilera);
    tabla.appendChild(tblHeader);

    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            myObj = this.responseText.split(/\r?\n/);
            var celda = document.createElement("td");
            var hilera = document.createElement("tr");
            var imagen = document.createElement('img');
            for (var x = myObj.length - 2; x >= 0; x--) {

                var salidaobjeto = JSON.parse(myObj[x]);
                imagen = document.createElement('img');
                celda = document.createElement("td");
                hilera = document.createElement("tr");
                var textoCelda = document.createTextNode(x);
                celda.appendChild(textoCelda);
                hilera.appendChild(celda);
                celda = document.createElement("td");
                imagen.src = salidaobjeto.nombre;
                celda.appendChild(imagen);
                hilera.appendChild(celda);
                var celda = document.createElement("td");
                var parrafo = document.createElement("p");
                var textoobjeto = "Pieza sin defectos";
                if (salidaobjeto.errores !== "") {
                    textoobjeto = salidaobjeto.errores;
                }
                var info = "<strong> Material: </strong> \n" + salidaobjeto.composicion +
                        "<br> <strong> Forma: </strong> \n" + salidaobjeto.forma +
                        " <br> <strong>Color:</strong> " + salidaobjeto.color +
                        " <br> <strong>Fallos:</strong> " + textoobjeto;
                var textoCelda = document.createTextNode(info);
                parrafo.innerHTML = info;
                celda.appendChild(parrafo);
                hilera.appendChild(celda);
                celda = document.createElement("td");
                var d = new Date(salidaobjeto.fecha);
                var textoCelda = document.createTextNode(d.toLocaleDateString("es-es", options));
                celda.appendChild(textoCelda);
                hilera.appendChild(celda);
                tblBody.appendChild(hilera);
            }
            tabla.appendChild(tblBody);
            body.appendChild(tabla);
        }
    };
    xmlhttp.open("GET", "/images/imagenes.json", true);
    xmlhttp.send();
}
function caracteristicaspiezas() {
    if (historicopiezas.value === "composicion") {
        estadisticoComposicion();
    } else if (historicopiezas.value === "turno") {
        console.log("Pendiente");
    } else if (historicopiezas.value === "color") {
        estadisticoColor();
    } else if (historicopiezas.value === "forma") {
        estadisticoForma();
    }




}
function cambio() {
    if (opcion.value === "hi") {
        historicopiezas.hidden = true;
        dibujar();
    } else if (opcion.value === "he") {
        historicopiezas.hidden = true;
        dibujartablaemergencia();
    } else if (opcion.value === "ha") {
        historicopiezas.hidden = true;
        dibujartablaalarma();
    } else if (opcion.value === "hg") {
        historicopiezas.hidden = false;
        if (historicopiezas.value === "composicion") {
            estadisticoComposicion();
        } else if (historicopiezas.value === "turno") {
            console.log("Pendiente");
        } else if (historicopiezas.value === "color") {
            estadisticoColor();
        } else if (historicopiezas.value === "forma") {
            estadisticoForma();
        }
    }
}

function dibujartablaemergencia() {
    var tabla = document.createElement("table");
    tabla.setAttribute("class", "w3-table w3-striped w3-bordered");
    tabla.setAttribute("id", "tablahistorial");
    var body = document.getElementById("divtabla");
    body.innerHTML = "";
    var tblBody = document.createElement("tbody");
    var tblHeader = document.createElement("thead");
    var hilera = document.createElement("tr");
    var textoCelda = document.createTextNode("Evento");
    var celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    var textoCelda = document.createTextNode("Tipo");
    celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    var textoCelda = document.createTextNode("Fecha");
    celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    tblHeader.appendChild(hilera);
    tabla.appendChild(tblHeader);
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var a = this.responseText.split(';');
            var j = 0;
            var hilera = document.createElement("tr");
            for (var i = a.length - 2; i >= 0; i--) {
                if (j === 2) {
                    var d = new Date(a[i]);
                    var textoCelda = document.createTextNode(d.toLocaleDateString("es-es", options));
                    var celda = document.createElement("td");
                    celda.appendChild(textoCelda);
                    hilera.appendChild(celda);
                    tblBody.appendChild(hilera);
                    var hilera = document.createElement("tr");
                    j = 0;
                } else {
                    var textoCelda = document.createTextNode(a[i]);
                    var celda = document.createElement("td");
                    celda.appendChild(textoCelda);
                    hilera.appendChild(celda);
                    j += 1;
                }
            }
            tabla.appendChild(tblBody);
            body.appendChild(tabla);
        }
    };
    xmlhttp.open("GET", "emergencia.csv", true);
    xmlhttp.send();
}

function dibujartablaalarma() {
    var body = document.getElementById("divtabla");
    body.innerHTML = "";
    var tabla = document.createElement("table");
    tabla.setAttribute("class", "w3-table w3-striped w3-bordered");
    tabla.setAttribute("id", "tablahistorial");
    var tblBody = document.createElement("tbody");
    var tblHeader = document.createElement("thead");
    var hilera = document.createElement("tr");
    var textoCelda = document.createTextNode("Evento");
    var celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    var textoCelda = document.createTextNode("Tipo");
    celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    var textoCelda = document.createTextNode("Fecha");
    celda = document.createElement("td");
    celda.appendChild(textoCelda);
    hilera.appendChild(celda);
    tblHeader.appendChild(hilera);
    tabla.appendChild(tblHeader);
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var a = this.responseText.split(';');
            var j = 0;
            var hilera = document.createElement("tr");
            for (var i = a.length - 2; i >= 0; i--) {
                if (j === 2) {
                    var d = new Date(a[i].toString());
                    var textoCelda = document.createTextNode(d.toLocaleDateString("es-es", options));
                    var celda = document.createElement("td");
                    celda.appendChild(textoCelda);
                    hilera.appendChild(celda);
                    tblBody.appendChild(hilera);
                    var hilera = document.createElement("tr");
                    j = 0;
                } else {
                    var textoCelda = document.createTextNode(a[i]);
                    var celda = document.createElement("td");
                    celda.appendChild(textoCelda);
                    hilera.appendChild(celda);
                    j += 1;
                }
            }
            tabla.appendChild(tblBody);
            body.appendChild(tabla);
        }
    };
    xmlhttp.open("GET", "alarmas.csv", true);
    xmlhttp.send();

}

socket.on('imagenprocesada', function (data) {
    if (opcion.value === "hi")
        dibujar();
    else if (opcion.value === "hg") {
        if (historicopiezas.value === "composicion") {
            estadisticoComposicion();
        } else if (historicopiezas.value === "turno") {
            console.log("Pendiente");
        } else if (historicopiezas.value === "color") {
            estadisticoColor();
        } else if (historicopiezas.value === "forma") {
            estadisticoForma();
        }
    }
});
socket.on('valores', function (data) {
    if (data.nombre === "Emergencia") {
        if (opcion.value === "he") {
            dibujartablaemergencia();
        }
    } else if (data.nombre === "Alerta") {
        if (opcion.value === "ha") {
            dibujartablaalarma()();
        }
    }
});