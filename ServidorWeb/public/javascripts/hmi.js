var socket = io();

var indicadorInductivo = document.getElementById("sensorInductivo");
var indicadorSensor1 = document.getElementById("sensorPresencia1");
var indicadorSensor2 = document.getElementById("sensorPresencia2");
var indicadorSensor3 = document.getElementById("sensorPresencia3");
var indicadorSensor4 = document.getElementById("sensorPresencia4");
var imagenProcesada = document.getElementById("imagenProcesada");
var indicadorPiston1 = document.getElementById("indicadorPiston1");
var indicadorPiston2 = document.getElementById("indicadorPiston2");
var ciclospiston1 = document.getElementById("ciclosPiston1");
var ciclospiston2 = document.getElementById("ciclosPiston2");
var textobandeja1 = document.getElementById("textobandeja1");
var textobandeja2 = document.getElementById("textobandeja2");
var indicadorEncendido = document.getElementById("Encendido");
var indicadorparoEmergencia = document.getElementById("paroEmergencia");
var textomodooperacion = document.getElementById("textomodooperacion");
var contenedorautenticacion = document.getElementById("autenticacion");
var contenedormanual = document.getElementById("manual");
var usuario = document.getElementById("usuario");
var password = document.getElementById("password");
var modooperacion = document.getElementById("modooperacion");
var bandeja = document.getElementById("bandeja");
var pieza = document.getElementById("pieza");
var contenedorsvg = document.getElementById("contenedorsvg");
var svg = document.getElementById("svg8");
var textoVelocidadMotor = document.getElementById("velocidadmotor");
var xmlhttp = new XMLHttpRequest();
var myObj = {};
var modo = "cmnm";
var bandeja1 = "Metal";
var bandeja2 = "No metal";
var aux = "e0e2560ad2a14559ef8a4c4f57b2c4f8";
var botonIniciar = document.getElementById("botonIniciar");
var botonParar = document.getElementById("botonParar");
var estadoIniciar = false;
var estadoParar = false;
var productofinal = false;
var datosprocesamiento = {};
var dispositivo = {};
var textocomposicion = document.getElementById("textocomposicion");
var textoforma = document.getElementById("textoforma");
var textocolor = document.getElementById("textocolor");
var textoestado = document.getElementById("textoestado");
var textoreceta = document.getElementById("textoreceta");

function reorientarsvg() {
    var w = window.outerWidth;
    var h = window.outerHeight;
    svg.setAttribute("height", h - h / 5);
    svg.setAttribute("width", w - w / 5);
}
var comandos = {}; //Comandos para enviar datoss
socket.on('valores', function (data) { //get button status from client
    if (data.nombre === "sensorPresencia1") {
        if (data.valor) {
            productofinal = false;
            indicadorSensor1.style.fill = 'green';
            pieza.setAttribute("x", "700");
            pieza.setAttribute("y", "22");
        } else
            indicadorSensor1.style.fill = 'white';
    } else if (data.nombre === "sensorPresencia2") {
        if (data.valor) {
            indicadorSensor2.style.fill = 'green';
            if (!productofinal) {
                pieza.setAttribute("x", "253");
                pieza.setAttribute("y", "22");
            }
        } else
            indicadorSensor2.style.fill = 'white';
    } else if (data.nombre === "sensorInductivo") {
        if (data.valor) {
            indicadorInductivo.style.fill = 'green';
            pieza.setAttribute("x", "615");
            pieza.setAttribute("y", "22");
        } else
            indicadorInductivo.style.fill = 'white';
    } else if (data.nombre === "sensorPresencia3") {
        if (data.valor) {
            indicadorSensor3.style.fill = 'green';
            if (!productofinal) {
                pieza.setAttribute("x", "359");
                pieza.setAttribute("y", "22");
            }
        } else
            indicadorSensor3.style.fill = 'white';
    } else if (data.nombre === "sensorPresencia4") {
        if (data.valor) {
            indicadorSensor4.style.fill = 'green';
            pieza.setAttribute("x", "545");
            pieza.setAttribute("y", "22");
        } else
            indicadorSensor4.style.fill = 'white';
    } else if (data.nombre === "Emergencia") {
        indicadorparoEmergencia.style.fill = "red";
    } else if (data.nombre === "liberar") {
        indicadorparoEmergencia.style.fill = "gray";
    } else if (data.nombre === "Piston1") {
        if (data.valor) {
            reseteardatospiston();
        }
    } else if (data.nombre === "Piston2") {
        if (data.valor) {
            reseteardatospiston();
        }
    } else if (data.nombre === "velocidadBanda") {
        textoVelocidadMotor.innerHTML = (parseInt(data.valor) * 100 / 5).toString() + "%";
    }
});
function reseteardatospiston() {
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            myObj = JSON.parse(this.responseText);
            ciclospiston1.innerHTML = myObj.ciclopiston1;
            ciclospiston2.innerHTML = myObj.ciclopiston2;
        }
    };
    xmlhttp.open("GET", "/ciclosuso.json", true);
    xmlhttp.send();
}

socket.on('modo', function (data) {
    console.log(data);
    bandeja1 = data.bandeja1;
    bandeja2 = data.bandeja2;
    modo = data.modo;
    textobandeja1.innerHTML = bandeja1;
    textobandeja2.innerHTML = bandeja2;
    console.log(String(bandeja1) + "/" + String(bandeja2));
    textoreceta.innerHTML = String(bandeja1) + "/" + String(bandeja2);
    if (modo === "cmnm")
        textomodooperacion.innerHTML = "Material";
    else if (modo === "cf")
        textomodooperacion.innerHTML = "Forma";
    if (modo === "cc")
        textomodooperacion.innerHTML = "Color";
    var x = bandeja.length;
    for (var i = 0; i <= x; i++) {
        bandeja.options.remove(toString(i));
    }
    if (modo === "cmnm") {
        modooperacion.selectedIndex = "0";
        var c = document.createElement("option");
        c.setAttribute("value", "Metal/No metal");
        var t = document.createTextNode("Metal/No metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "No metal/Metal");
        var t = document.createTextNode("No metal/Metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (bandeja1 + "/" + bandeja2 === "metal/nometal") {
            bandeja.selectedIndex = "0";
        } else {
            bandeja.selectedIndex = "1";
        }
    } else if (modo === "cf") {
        modooperacion.selectedIndex = "1";
        var c = document.createElement("option");
        c.setAttribute("value", "Circulo/Cuadrado");
        var t = document.createTextNode("Círculo/Cuadrado");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Cuadrado/Circulo");
        var t = document.createTextNode("Cuadrado/Círculo");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "Circulo/Cuadrado") {
            bandeja.selectedIndex = "0";
        } else {
            bandeja.selectedIndex = "1";
        }

    } else if (modo === "cc") {
        modooperacion.selectedIndex = "2";
        var c = document.createElement("option");
        c.setAttribute("value", "Verde/Otros");
        var t = document.createTextNode("Verde/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Gris/Otros");
        var t = document.createTextNode("Gris/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Amarillo/Otros");
        var t = document.createTextNode("Amarillo/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Verde");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Gris");
        var t = document.createTextNode("Otros/Gris");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Amarillo");
        var t = document.createTextNode("Otros/Amarillo");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "Verde/Otros") {
            bandeja.selectedIndex = "0";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Gris/Otros") {
            bandeja.selectedIndex = "1";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Amarillo/Otros") {
            bandeja.selectedIndex = "2";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Verde") {
            bandeja.selectedIndex = "3";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Gris") {
            bandeja.selectedIndex = "4";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Amarillo") {
            bandeja.selectedIndex = "5";
        }
    }
});
socket.on('imagenprocesada', function (data) { //get button status from client
    productofinal = true;
    datosprocesamiento = data;
    textoforma.innerHTML = data.forma;
    textocolor.innerHTML = data.color;
    textocomposicion.innerHTML = data.composicion;
    if (data.errores === "")
        textoestado.innerHTML = "Pieza sin defectos";
    else if (data.errores === "-")
        textoestado.innerHTML = "-";
    else
        textoestado.innerHTML = "Pieza defectuosa";
    if (datosprocesamiento.errores === "") {
        if (modo === "cmnm") {
            console.log(datosprocesamiento.composicion);
            if (datosprocesamiento.composicion === bandeja1) {
                pieza.setAttribute("x", "359");
                pieza.setAttribute("y", "150");
            } else if (datosprocesamiento.composicion === bandeja2) {
                pieza.setAttribute("x", "270");
                pieza.setAttribute("y", "150");
            }
        } else if (modo === "cc") {
            if (bandeja1 === "Otros") {
                if (datosprocesamiento.color === bandeja2) {
                    pieza.setAttribute("x", "270");
                    pieza.setAttribute("y", "150");
                } else {
                    pieza.setAttribute("x", "359");
                    pieza.setAttribute("y", "150");
                }
            } else {
                if (datosprocesamiento.color === bandeja1) {
                    pieza.setAttribute("x", "359");
                    pieza.setAttribute("y", "150");
                } else {
                    pieza.setAttribute("x", "270");
                    pieza.setAttribute("y", "150");
                }
            }
        } else if (modo === "cf") {
            if (datosprocesamiento.forma === bandeja1) {
                pieza.setAttribute("x", "359");
                pieza.setAttribute("y", "150");
            } else {
                pieza.setAttribute("x", "270");
                pieza.setAttribute("y", "150");
            }
        }

    } else {
        pieza.setAttribute("x", "60");
        pieza.setAttribute("y", "22");
    }

});


socket.on('estado', function (data) {
    console.log(data);
    if (data.estado === -1) {
        indicadorparoEmergencia.style.fill = "red";
        indicadorEncendido.style.fill = "gray";
    } else if (data.estado === 0) {
        indicadorparoEmergencia.style.fill = "gray";
        indicadorEncendido.style.fill = "gray";
    } else if (data.estado === 1) {
        indicadorparoEmergencia.style.fill = "gray";
        indicadorEncendido.style.fill = "green";
    }
});

function validar() {
    var a = usuario.value.length;
    var b = password.value.length;
    if (a > 6 || b > 6) {
        var cdn = password.value.charAt(parseInt(b / 2)) +
                usuario.value.charAt(0) +
                password.value.charAt(0) +
                usuario.value.charAt(a - 1) +
                password.value.charAt(b - 1) +
                usuario.value.charAt(parseInt(a / 2));
        var MD5 = new Hashes.MD5().hex(cdn);
        if (MD5 === aux) {
            contenedorautenticacion.hidden = true;
            contenedormanual.hidden = false;
            contenedorsvg.hidden = false;
        } else {
            alert("No coinciden el nombre de usuario y contraseña");
        }
    } else {
        alert("Nombre de usuario o contraseña debe contener al menos 6 dígitos");
    }
}
function usuarionormal() {
    contenedorsvg.hidden = false;
    contenedorautenticacion.hidden = true;
}
function cambio() {
    var x = 0;
    x = bandeja.length;
    for (var i = 0; i <= x; i++) {
        bandeja.options.remove(toString(i));
    }
    if (modooperacion.value === "cmnm") {
        x = bandeja.length;
        var c = document.createElement("option");
        c.setAttribute("value", "Metal/No metal");
        var t = document.createTextNode("Metal/ No metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "No metal/Metal");
        var t = document.createTextNode("No metal/Metal");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cf") {
        var c = document.createElement("option");
        c.setAttribute("value", "Circulo/Cuadrado");
        var t = document.createTextNode("Círculo/Cuadrado");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Cuadrado/Circulo");
        var t = document.createTextNode("Cuadrado/Círculo");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cc") {
        var c = document.createElement("option");
        c.setAttribute("value", "Verde/Otros");
        var t = document.createTextNode("Verde/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Gris/Otros");
        var t = document.createTextNode("Gris/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Amarillo/Otros");
        var t = document.createTextNode("Amarillo/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Verde");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Gris");
        var t = document.createTextNode("Otros/Gris");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Amarillo");
        var t = document.createTextNode("Otros/Amarillo");
        c.appendChild(t);
        bandeja.appendChild(c);
    }
    bandeja.selected = "0";
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    bandeja1 = dispositivo.bandeja1;
    bandeja2 = dispositivo.bandeja2;
    modo = modooperacion.value;
    textobandeja1.innerHTML = bandeja1;
    textobandeja2.innerHTML = bandeja2;
    if (modo === "cmnm")
        textomodooperacion.innerHTML = "Material";
    else if (modo === "cf")
        textomodooperacion.innerHTML = "Forma";
    if (modo === "cc")
        textomodooperacion.innerHTML = "Color";
    socket.emit('modo', dispositivo);
    textoreceta.innerHTML = bandeja1 + "/" + bandeja2;
    dispositivo = {};
}
function cambiobandejas() {
    var dispositivo = {};
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    socket.emit('modo', dispositivo);
    console.log(dispositivo);
    bandeja1 = dispositivo.bandeja1;
    bandeja2 = dispositivo.bandeja2;
    modo = modooperacion.value;
    textobandeja1.innerHTML = bandeja1;
    textobandeja2.innerHTML = bandeja2;
    if (modo === "cmnm")
        textomodooperacion.innerHTML = "Material";
    else if (modo === "cf")
        textomodooperacion.innerHTML = "Forma";
    if (modo === "cc")
        textomodooperacion.innerHTML = "Color";
    textoreceta.innerHTML = bandeja1 + "/" + bandeja2;

}

socket.on('estado', function (data) {
    if (data.estado === -1) {
        estadoParar = true;
        estadoIniciar = false;
        botonIniciar.value = "Iniciar";
        botonParar.value = "Desenclavar Paro";
    } else if (data.estado === 0) {
        estadoParar = false;
        estadoIniciar = false;
        botonIniciar.value = "Iniciar";
        botonParar.value = "Paro Emergencia";
    } else if (data.estado === 1) {
        estadoParar = false;
        estadoIniciar = true;
        botonIniciar.value = "Parar";
        botonParar.value = "Paro Emergencia";
    }
});
botonIniciar.addEventListener("click", function () {
    estadoIniciar = !estadoIniciar;
    var enviarIniciar = {};
    if (estadoIniciar) {
        enviarIniciar = {nombre: "estado", estado: 1};
        botonIniciar.value = "Parar";
        indicadorEncendido.style.fill = "green";
    } else {
        enviarIniciar = {nombre: "estado", estado: 0};
        botonIniciar.value = "Iniciar";
        indicadorEncendido.style.fill = "gray";
    }
    socket.emit("estado", enviarIniciar);
});
botonParar.addEventListener("click", function () {
    estadoParar = !estadoParar;
    var enviarParar = {};
    if (estadoParar) {
        enviarParar = {nombre: "estado", estado: -1};
        botonParar.value = "Desenclavar Paro";
        botonIniciar.disabled = true;
        indicadorEncendido.style.fill = "gray";
        indicadorparoEmergencia.style.fill = "red";
    } else {
        enviarParar = {nombre: "estado", estado: 0};
        botonParar.value = "Paro Emergencia";
        botonIniciar.disabled = false;
        indicadorEncendido.style.fill = "gray";
        indicadorparoEmergencia.style.fill = "gray";
    }
    socket.emit("estado", enviarParar);
});
password.addEventListener("keypress", function (event) {
    if (event.keyCode === 13) {
        validar();
    }
});
