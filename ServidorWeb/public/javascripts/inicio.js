var socket = io();
var xmlhttp = new XMLHttpRequest();
socket.on('valores', function (data) { //get button status from client
    if (data.nombre === "Emergencia" || data.nombre === "Alerta") {
        if (data.nombre === "Emergencia") {
            if (window.Notification && Notification.permission !== "denied") {
                Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                    var n = new Notification('Notificación de Emergencia', {
                        body: 'Se ha activado un evento de Emergencia! \nRevise la página de monitorización/historial para más detalles)',
                        icon: '/images/fallo.gif' // optional
                    });
                });
            }
        } else if (data.nombre === "Alerta") {
            if (window.Notification && Notification.permission !== "denied") {
                Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                    var n = new Notification('Notificación de Alerta', {
                        body: 'e ha activado un evento de Alerta!\n\nRevise la página de monitorización/historial para más detalles)',
                        icon: '/images/alerta.jpg' // optional
                    });
                });
            }
        }
    } else if (data.nombre === "Piston1") {
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                myObj = JSON.parse(this.responseText);
                if (myObj.ciclopiston1 > myObj.ciclosmaximospiston1) {
                    if (window.Notification && Notification.permission !== "denied") {
                        Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                            var n = new Notification('Alerta de Ciclo de Vida Piston 1', {
                                body: 'Tiempo de vida útil\nPistón 1 llegó al final de su vida útil)',
                                icon: '/images/alerta.jpg' // optional
                            });
                        });
                    }
                }
            }
        };
        xmlhttp.open("GET", "/ciclosuso.json", true);
        xmlhttp.send();
    } else if (data.nombre === "Piston2") {
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                myObj = JSON.parse(this.responseText);
                if (myObj.ciclopiston2 > myObj.ciclosmaximospiston2) {
                    if (window.Notification && Notification.permission !== "denied") {
                        Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                            var n = new Notification('Alerta de Ciclo de Vida Piston 2', {
                                body: 'Tiempo de vida útil\nPistón 2 llegó al final de su vida útil)',
                                icon: '/images/alerta.jpg' // optional
                            });
                        });
                    }
                }
            }
        };
        xmlhttp.open("GET", "/ciclosuso.json", true);
        xmlhttp.send();
    }
});


