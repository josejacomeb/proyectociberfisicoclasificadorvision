var socket = io();
var botonPiston1 = document.getElementById("botonPiston1");
var estadobotonPiston1 = false;
var botonPiston2 = document.getElementById("botonPiston2");
var indicadorInductivo = document.getElementById("sensorInductivo");
var indicadorSensor1 = document.getElementById("sensorPresencia1");
var indicadorSensor2 = document.getElementById("sensorPresencia2");
var indicadorSensor3 = document.getElementById("sensorPresencia3");
var indicadorSensor4 = document.getElementById("sensorPresencia4");
var ciclospiston1 = document.getElementById("ciclospiston1");
var ciclospiston2 = document.getElementById("ciclospiston2");
var maxciclospiston1 = document.getElementById("maxciclospiston1");
var maxciclospiston2 = document.getElementById("maxciclospiston2");
var botonIniciar = document.getElementById("botonIniciar");
var botonParar = document.getElementById("botonParar");
var contenedorautenticacion = document.getElementById("autenticacion");
var contenedormanual = document.getElementById("manual");
var usuario = document.getElementById("usuario");
var password = document.getElementById("password");
var modooperacion = document.getElementById("modooperacion");
var bandeja = document.getElementById("bandeja");
var estadobotonPiston2 = false;
var comandos = {}; //Comandos para enviar datos
var aux = "e0e2560ad2a14559ef8a4c4f57b2c4f8";
var estadoIniciar = false;
var estadoParar = false;
var dispositivo = {};

var xmlhttp = new XMLHttpRequest();
password.addEventListener("keypress", function (event) {
    if (event.keyCode === 13) {
        validar();
    }
});

botonPiston1.addEventListener("click", function () {
    estadobotonPiston1 = !estadobotonPiston1;
    if (estadobotonPiston1) {
        botonPiston1.value = "Activado";
    } else
        botonPiston1.value = "Desactivado";
    comandos = {};
    comandos.nombre = "Piston1";
    comandos.valor = estadobotonPiston1;
    socket.emit('valores', comandos);
});
botonPiston2.addEventListener("click", function () {
    estadobotonPiston2 = !estadobotonPiston2;
    if (estadobotonPiston2) {
        botonPiston2.value = "Activado";
    } else
        botonPiston2.value = "Desactivado";
    comandos = {};
    comandos.nombre = "Piston2";
    comandos.valor = estadobotonPiston2;
    socket.emit('valores', comandos);
});
botonIniciar.addEventListener("click", function () {
    estadoIniciar = !estadoIniciar;
    var enviarIniciar = {};
    if (estadoIniciar) {
        enviarIniciar = {nombre: "estado", estado: 1};
        botonIniciar.value = "Parar";

    } else {
        enviarIniciar = {nombre: "estado", estado: 0};
        botonIniciar.value = "Iniciar";
    }
    socket.emit("estado", enviarIniciar);
    console.log(enviarIniciar);
});
botonParar.addEventListener("click", function () {
    estadoParar = !estadoParar;
    var enviarParar = {};
    if (estadoParar) {
        enviarParar = {nombre: "estado", estado: -1};
        botonParar.value = "Desenclavar Paro";
        botonIniciar.disabled = true;
    } else {
        enviarParar = {nombre: "estado", estado: 0};
        botonParar.value = "Paro Emergencia";
        botonIniciar.disabled = false;
    }
    socket.emit("estado", enviarParar);
    console.log(enviarParar);
});


function validar() {
    var a = usuario.value.length;
    var b = password.value.length;
    if (a > 6 || b > 6) {
        var cdn = password.value.charAt(parseInt(b / 2)) +
                usuario.value.charAt(0) +
                password.value.charAt(0) +
                usuario.value.charAt(a - 1) +
                password.value.charAt(b - 1) +
                usuario.value.charAt(parseInt(a / 2));
        var MD5 = new Hashes.MD5().hex(cdn);
        if (MD5 === aux) {
            contenedormanual.hidden = false;
            contenedorautenticacion.hidden = true;
        } else {
            alert("No coinciden el nombre de usuario y contraseña");
        }
    } else {
        alert("Nombre de usuario o contraseña debe contener al menos 6 dígitos");
    }
}
socket.on('valores', function (data) {
    if (data.nombre === "Piston1" && data.nombre === "Piston2") {
        reseteardatospiston();
    }
    else if (data.nombre === "sensorPresencia1") {
        if (data.valor) {
            indicadorSensor1.src = "images/check.png";
            indicadorSensor1.alt = "Activado";
        } else {
            indicadorSensor1.src = "images/nocheck.png";
            indicadorSensor1.alt = "Desactivado";
        }
    } else if (data.nombre === "sensorPresencia2") {
        if (data.valor) {
            indicadorSensor2.src = "images/check.png";
            indicadorSensor2.alt = "Activado";
        } else {
            indicadorSensor2.src = "images/nocheck.png";
            indicadorSensor2.alt = "Desactivado";
        }
    } else if (data.nombre === "sensorInductivo") {
        if (data.valor) {
            indicadorInductivo.src = "images/check.png";
            indicadorInductivo.alt = "Activado";
        } else {
            indicadorInductivo.src = "images/nocheck.png";
            indicadorInductivo.alt = "Desactivado";
        }
    } else if (data.nombre === "sensorPresencia3") {
        if (data.valor) {
            indicadorSensor3.src = "images/check.png";
            indicadorSensor3.alt = "Activado";
        } else {
            indicadorSensor3.src = "images/nocheck.png";
            indicadorSensor3.alt = "Desactivado";
        }
    } else if (data.nombre === "sensorPresencia4") {
        if (data.valor) {
            indicadorSensor4.src = "images/check.png";
            indicadorSensor4.alt = "Activado";
        } else {
            indicadorSensor4.src = "images/nocheck.png";
            indicadorSensor4.alt = "Desactivado";
        }
    }

});
socket.on('modo', function (data) {
    if (data.nombre === "Piston1" && data.nombre === "Piston2") {
        reseteardatospiston();
    }
});
socket.on('estado', function (data) {
    if (data.estado === -1) {
        estadoParar = true;
        estadoIniciar = false;
        botonIniciar.value = "Iniciar";
        botonParar.value = "Desenclavar Paro";
    } else if (data.estado === 0) {
        estadoParar = false;
        estadoIniciar = false;
        botonIniciar.value = "Iniciar";
        botonParar.value = "Paro Emergencia";
    } else if (data.estado === 1) {
        estadoParar = false;
        estadoIniciar = true;
        botonIniciar.value = "Parar";
        botonParar.value = "Paro Emergencia";
    }
});
function cambio() {
    var x = 0;
    x = bandeja.length;
    for (var i = 0; i <= x; i++) {
        bandeja.options.remove(toString(i));
    }
    if (modooperacion.value === "cmnm") {
        x = bandeja.length;
        var c = document.createElement("option");
        c.setAttribute("value", "Metal/No metal");
        var t = document.createTextNode("Metal/No metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "No metal/Metal");
        var t = document.createTextNode("No metal/Metal");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cf") {
        var c = document.createElement("option");
        c.setAttribute("value", "Circulo/Cuadrado");
        var t = document.createTextNode("Círculo/Cuadrado");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Cuadrado/Circulo");
        var t = document.createTextNode("Cuadrado/Círculo");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cc") {
        var c = document.createElement("option");
        c.setAttribute("value", "Verde/Otros");
        var t = document.createTextNode("Verde/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Gris/Otros");
        var t = document.createTextNode("Gris/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Amarillo/Otros");
        var t = document.createTextNode("Amarillo/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Verde");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Gris");
        var t = document.createTextNode("Otros/Gris");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Amarillo");
        var t = document.createTextNode("Otros/Amarillo");
        c.appendChild(t);
        bandeja.appendChild(c);
    }
    bandeja.selected = "0";
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    socket.emit('modo', dispositivo);
    dispositivo = {};
}
function cambiobandejas() {
    var dispositivo = {};
    console.log("Cadenas");
    console.log(cadenas);
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    socket.emit('modo', dispositivo);
}

socket.on('modo', function (data) {
    var x = bandeja.length;
    for (var i = 0; i <= x; i++) {
        bandeja.options.remove(toString(i));
    }
    if (data.modo === "cmnm") {
        modooperacion.selectedIndex = "0";
        var c = document.createElement("option");
        c.setAttribute("value", "Metal/No metal");
        var t = document.createTextNode("Metal/No metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "No metal/Metal");
        var t = document.createTextNode("No metal/Metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "metal/nometal") {
            bandeja.selectedIndex = "0";
        } else {
            bandeja.selectedIndex = "1";
        }
    } else if (data.modo === "cf") {
        modooperacion.selectedIndex = "1";
        var c = document.createElement("option");
        c.setAttribute("value", "Circulo/Cuadrado");
        var t = document.createTextNode("Círculo/Cuadrado");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Cuadrado/Circulo");
        var t = document.createTextNode("Cuadrado/Círculo");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "Circulo/Cuadrado") {
            bandeja.selectedIndex = "0";
        } else {
            bandeja.selectedIndex = "1";
        }

    } else if (data.modo === "cc") {
        modooperacion.selectedIndex = "2";
        var c = document.createElement("option");
        c.setAttribute("value", "Verde/Otros");
        var t = document.createTextNode("Verde/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Gris/Otros");
        var t = document.createTextNode("Gris/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Amarillo/Otros");
        var t = document.createTextNode("Amarillo/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Verde");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Gris");
        var t = document.createTextNode("Otros/Gris");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Amarillo");
        var t = document.createTextNode("Otros/Amarillo");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "Verde/Otros") {
            bandeja.selectedIndex = "0";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Gris/Otros") {
            bandeja.selectedIndex = "1";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Amarillo/Otros") {
            bandeja.selectedIndex = "2";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Verde") {
            bandeja.selectedIndex = "3";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Gris") {
            bandeja.selectedIndex = "4";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Amarillo") {
            bandeja.selectedIndex = "5";
        }
    }
});
