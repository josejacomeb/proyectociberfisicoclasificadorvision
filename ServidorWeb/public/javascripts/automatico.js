var socket = io();
var botonPiston1 = document.getElementById("botonPiston1");
var estadobotonPiston1 = false;
var estadobotonPiston2 = false;
var botonPiston2 = document.getElementById("botonPiston2");
var indicadorInductivo = document.getElementById("sensorInductivo");
var indicadorSensor1 = document.getElementById("sensorPresencia1");
var indicadorSensor2 = document.getElementById("sensorPresencia2");
var indicadorSensor3 = document.getElementById("sensorPresencia3");
var indicadorSensor4 = document.getElementById("sensorPresencia4");
var imagenProcesada = document.getElementById("imagenProcesada");
var indicadorPiston1 = document.getElementById("indicadorPiston1");
var indicadorPiston2 = document.getElementById("indicadorPiston2");
var imagen = document.getElementById("imagenProcesada");
var informacion = document.getElementById("informacion");
var labelpiston1 = document.getElementById("labelpiston1");
var labelpiston2 = document.getElementById("labelpiston2");
var modooperacion = document.getElementById("modooperacion");
var bandeja = document.getElementById("bandeja");
var imgfallo = document.getElementById("fallo");
var clasificacion = document.getElementById("clasificacion");
var infofalla = document.getElementById("descripcionfalla");
var contenedorfalla = document.getElementById("contenedorfalla");

var xmlhttp = new XMLHttpRequest();
var myObj = {};
var dispositivo = {};



var comandos = {}; //Comandos para enviar datoss
console.log("Inicio del Script");
function cambio() {
    var x = 0;
    x = bandeja.length;
    console.log(x);
    for (var i = 0; i <= x; i++) {
        bandeja.options.remove(toString(i));
    }
    if (modooperacion.value === "cmnm") {
        x = bandeja.length;
        var c = document.createElement("option");
        c.setAttribute("value", "metal/nometal");
        var t = document.createTextNode("Metal/ No metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "No metal/Metal");
        var t = document.createTextNode("No metal/Metal");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cf") {
        var c = document.createElement("option");
        c.setAttribute("value", "Circulo/Cuadrado");
        var t = document.createTextNode("Círculo/Cuadrado");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Cuadrado/Circulo");
        var t = document.createTextNode("Cuadrado/Círculo");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cc") {
        var c = document.createElement("option");
        c.setAttribute("value", "Verde/Otros");
        var t = document.createTextNode("Verde/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Gris/Otros");
        var t = document.createTextNode("Gris/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Amarillo/Otros");
        var t = document.createTextNode("Amarillo/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Verde");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Gris");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Amarillo");
        var t = document.createTextNode("Otros/Amarillo");
        c.appendChild(t);
        bandeja.appendChild(c);
    }
    bandeja.selected = "0";
    console.log("Cambio");
    console.log(bandeja.value);
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    socket.emit('modo', dispositivo);
    console.log(dispositivo);
    dispositivo = {};
}
function cambiobandejas() {
    var dispositivo = {};
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    socket.emit('modo', dispositivo);
}
socket.on('valores', function (data) { //get button status from client
    if (data.nombre === "Emergencia" || data.nombre === "Alerta") {
        contenedorfalla.hidden = false;
        if (data.nombre === "Emergencia") {
            if (window.Notification && Notification.permission !== "denied") {
                Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                    var n = new Notification('Notificación de Emergencia', {
                        body: 'Se ha activado un evento de Emergencia! \nRevise la página de monitorización/historial para más detalles)',
                        icon: '/images/fallo.gif' // optional
                    });
                });
            }
            contenedorfalla.setAttribute("class", "w3-red");
            imgfallo.src = "images/fallo.gif";
            imgfallo.style = "width:10%";
            imgfallo.alt = "Error";
            infofalla.innerHTML = data.valor;
        } else if (data.nombre === "Alerta") {
            if (window.Notification && Notification.permission !== "denied") {
                Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                    var n = new Notification('Notificación de Alerta', {
                        body: 'e ha activado un evento de Alerta!\n\nRevise la página de monitorización/historial para más detalles)',
                        icon: '/images/alerta.jpg' // optional
                    });
                });
            }
            contenedorfalla.setAttribute("class", "w3-yellow");
            imgfallo.src = "images/alerta.png";
            imgfallo.style.width = "width:10%";
            imgfallo.alt = "Alerta";
            infofalla.innerHTML = data.valor;
        }
    } else if (data.nombre === "liberar") {
        imgfallo.src = "images/nocheck.png";
        imgfallo.style.width = "width:0%";
        imgfallo.alt = "Sin novedad";
        infofalla.innerHTML = "";
        contenedorfalla.hidden = true;
    } else {
        if (data.nombre === "sensorPresencia1") {
            if (data.valor) {
                indicadorSensor1.src = "images/check.png";
                indicadorSensor1.alt = "Activado";
            } else {
                indicadorSensor1.src = "images/nocheck.png";
                indicadorSensor1.alt = "Desactivado";
            }
        } else if (data.nombre === "sensorPresencia2") {
            if (data.valor) {
                indicadorSensor2.src = "images/check.png";
                indicadorSensor2.alt = "Activado";
            } else {
                indicadorSensor2.src = "images/nocheck.png";
                indicadorSensor2.alt = "Desactivado";
            }
        } else if (data.nombre === "sensorInductivo") {
            if (data.valor) {
                indicadorInductivo.src = "images/check.png";
                indicadorInductivo.alt = "Activado";
            } else {
                indicadorInductivo.src = "images/nocheck.png";
                indicadorInductivo.alt = "Desactivado";
            }
        } else if (data.nombre === "sensorPresencia3") {
            if (data.valor) {
                indicadorSensor3.src = "images/check.png";
                indicadorSensor3.alt = "Activado";
            } else {
                indicadorSensor3.src = "images/nocheck.png";
                indicadorSensor3.alt = "Desactivado";
            }
        } else if (data.nombre === "sensorPresencia4") {
            if (data.valor) {
                indicadorSensor4.src = "images/check.png";
                indicadorSensor4.alt = "Activado";
            } else {
                indicadorSensor4.src = "images/nocheck.png";
                indicadorSensor4.alt = "Desactivado";
            }
        } else if (data.nombre === "Piston1") {
            xmlhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    myObj = JSON.parse(this.responseText);
                    if (myObj.ciclopiston1 > myObj.ciclosmaximospiston1) {
                        dispositivo = {};
                        dispositivo.nombre = "Alerta";
                        dispositivo.valor = "Fin de ciclo de vida de piston 1";
                        socket.emit("valores", dispositivo);
                        dispositivo = {};
                        contenedorfalla.hidden = false;
                        contenedorfalla.setAttribute("class", "w3-yellow");
                        imgfallo.src = "images/alerta.ng";
                        imgfallo.style = "width:10%";
                        imgfallo.alt = "Error";
                        infofalla.innerHTML = "Piston 1 al final de su vida útil, considere reemplazarlo";
                        if (window.Notification && Notification.permission !== "denied") {
                            Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                                var n = new Notification('Alerta de Ciclo de Vida Piston 1', {
                                    body: 'Tiempo de vida útil\nPistón 1 llegó al final de su vida útil)',
                                    icon: '/images/alerta.jpg' // optional
                                });
                            });
                        }
                    }
                }
                labelpiston1.innerHTML = "Piston1(" + myObj.ciclopiston1 + "ciclos)";
            };
            xmlhttp.open("GET", "/ciclosuso.json", true);
            xmlhttp.send();
            if (data.valor) {
                indicadorPiston1.src = "images/pe.png";
                indicadorPiston1.alt = "Piston 1 Extraido";
            } else {
                indicadorPiston1.src = "images/pr.png";
                indicadorPiston1.alt = "Piston 1 retraido";
            }
        } else if (data.nombre === "Piston2") {
            xmlhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    myObj = JSON.parse(this.responseText);
                    labelpiston2.innerHTML = "Piston2(" + myObj.ciclopiston2 + "ciclos)";
                    if (myObj.ciclopiston2 > myObj.ciclosmaximospiston2) {
                        dispositivo = {};
                        dispositivo.nombre = "Alerta";
                        dispositivo.valor = "Fin de ciclo de vida de piston 2";
                        socket.emit("valores", dispositivo);
                        dispositivo = {};
                        contenedorfalla.hidden = false;
                        contenedorfalla.setAttribute("class", "w3-yellow");
                        imgfallo.src = "images/alerta.ng";
                        imgfallo.style = "width:10%";
                        imgfallo.alt = "Error";
                        infofalla.innerHTML = "Piston 2 al final de su vida útil, considere reemplazarlo";
                        if (window.Notification && Notification.permission !== "denied") {
                            Notification.requestPermission(function (status) {  // status is "granted", if accepted by user
                                var n = new Notification('Alerta de Ciclo de Vida Piston 2', {
                                    body: 'Tiempo de vida útil\nPistón 2 llegó al final de su vida útil)',
                                    icon: '/images/alerta.jpg' // optional
                                });
                            });
                        }
                    }
                }
            };
            xmlhttp.open("GET", "/ciclosuso.json", true);
            xmlhttp.send();
            if (data.valor) {
                indicadorPiston2.src = "images/pe.png";
                indicadorPiston2.alt = "Piston 2 Extraido";
            } else {
                indicadorPiston2.src = "images/pr.png";
                indicadorPiston2.alt = "Piston 2 retraido";
            }
        }
    }
});
socket.on('imagenprocesada', function (data) {
    console.log(data);
    imagen.src = data.nombre;
    imagen.alt = data.nombre;
    var textofallo = "Pieza sin defectos";
    if (data.errores !== "") {
        textofallo = data.errores;
    }
    informacion.innerHTML = "<strong> Material: </strong>" + data.composicion + "<br>" + "<strong> Forma: </strong>" + data.forma + "<br> <strong>Color:</strong> " + data.color + "<br> <strong>Fallos:</strong> " + textofallo;

});

socket.on('modo', function (data) {
    if (data.modo === "cmnm") {
        clasificacion.innerHTML = "<strong>Clasificación por material</strong> Bandeja1: " + data.bandeja1 + ", Bandeja 2: " + data.bandeja2;
    } else if (data.modo === "cf") {
        clasificacion.innerHTML = "<strong>Clasificación por forma</strong> Bandeja1: " + data.bandeja1 + ", Bandeja 2: " + data.bandeja2;
    } else if (data.modo === "cc") {
        clasificacion.innerHTML = "<strong>Clasificación por colores</strong> Bandeja1: " + data.bandeja1 + ", Bandeja 2: " + data.bandeja2;
    }
});


