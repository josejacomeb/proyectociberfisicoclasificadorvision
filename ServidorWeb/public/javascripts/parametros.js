var socket = io();
var indicadorInductivo = document.getElementById("sensorInductivo");
var indicadorSensor1 = document.getElementById("sensorPresencia1");
var indicadorSensor2 = document.getElementById("sensorPresencia2");
var indicadorSensor3 = document.getElementById("sensorPresencia3");
var indicadorSensor4 = document.getElementById("sensorPresencia4");
var ciclospiston1 = document.getElementById("ciclospiston1");
var ciclospiston2 = document.getElementById("ciclospiston2");
var maxciclospiston1 = document.getElementById("maxciclospiston1");
var maxciclospiston2 = document.getElementById("maxciclospiston2");
var botonEnviar = document.getElementById("botonEnviar");
var botonResetear = document.getElementById("botonResetear");
var contenedorautenticacion = document.getElementById("autenticacion");
var contenedormanual = document.getElementById("manual");
var usuario = document.getElementById("usuario");
var password = document.getElementById("password");
var modooperacion = document.getElementById("modooperacion");
var bandeja = document.getElementById("bandeja");
var correo = document.getElementById("correo");
var botonCorreo = document.getElementById("botonCorreo");
var velocidadBanda = document.getElementById("velocidadBanda");
var sliderBanda = document.getElementById("sliderBanda");
var comandos = {}; //Comandos para enviar datos
var aux = "e0e2560ad2a14559ef8a4c4f57b2c4f8";
var estadoIniciar = false;
var estadoParar = false;
var dispositivo = {};

var xmlhttp = new XMLHttpRequest();
password.addEventListener("keypress", function (event) {
    if (event.keyCode === 13) {
        validar();
    }
});

botonResetear.addEventListener("click", function () {
    reseteardatospiston();
});
botonEnviar.addEventListener("click", function () {
    comandos = {};
    comandos.ciclopiston1 = parseInt(ciclospiston1.value);
    comandos.ciclopiston2 = parseInt(ciclospiston2.value);
    comandos.ciclosmaximospiston1 = parseInt(maxciclospiston1.value);
    comandos.ciclosmaximospiston2 = parseInt(maxciclospiston2.value);
    socket.emit('ciclouso', comandos);
});
botonCorreo.addEventListener("click", function () {
    var cadenacorreo = correo.value;
    console.log(cadenacorreo.search("@"));
    console.log(cadenacorreo.search("."));
    if (cadenacorreo.search("@")) {
        var enviarCadena = {};
        enviarCadena.nombre = "Correo";
        enviarCadena.valor = cadenacorreo;
        socket.emit("valores", enviarCadena);

    } else {
        alert("Ingrese un correo de contacto válido");
    }
});

function reseteardatospiston() {
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            myObj = JSON.parse(this.responseText);
            ciclospiston1.value = myObj.ciclopiston1;
            ciclospiston2.value = myObj.ciclopiston2;
            maxciclospiston1.value = myObj.ciclosmaximospiston1;
            maxciclospiston2.value = myObj.ciclosmaximospiston2;
        }
    };
    xmlhttp.open("GET", "/ciclosuso.json", true);
    xmlhttp.send();
}

function validar() {
    var a = usuario.value.length;
    var b = password.value.length;
    if (a > 6 || b > 6) {
        var cdn = password.value.charAt(parseInt(b / 2)) +
                usuario.value.charAt(0) +
                password.value.charAt(0) +
                usuario.value.charAt(a - 1) +
                password.value.charAt(b - 1) +
                usuario.value.charAt(parseInt(a / 2));
        var MD5 = new Hashes.MD5().hex(cdn);
        if (MD5 === aux) {
            contenedormanual.hidden = false;
            contenedorautenticacion.hidden = true;
            reseteardatospiston();
        } else {
            alert("No coinciden el nombre de usuario y contraseña");
        }
    } else {
        alert("Nombre de usuario o contraseña debe contener al menos 6 dígitos");
    }
}
function datosbanda() {
    comandos = {};
    velocidadBanda.innerHTML = "Velocidad banda(" + (parseInt(sliderBanda.value) * 100 / 5).toString() + "%)";
    comandos.nombre = "velocidadBanda";
    comandos.valor = sliderBanda.value;
    socket.emit('valores', comandos);
    comandos = {};
}
socket.on('valores', function (data) {
    if (data.nombre === "Piston1" && data.nombre === "Piston2") {
        reseteardatospiston();
    }
});
socket.on('modo', function (data) {
    if (data.nombre === "Piston1" && data.nombre === "Piston2") {
        reseteardatospiston();
    }
});

function cambio() {
    var x = 0;
    x = bandeja.length;
    for (var i = 0; i <= x; i++) {
        bandeja.options.remove(toString(i));
    }
    if (modooperacion.value === "cmnm") {
        x = bandeja.length;
        var c = document.createElement("option");
        c.setAttribute("value", "Metal/No metal");
        var t = document.createTextNode("Metal/No metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "No metal/Metal");
        var t = document.createTextNode("No metal/Metal");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cf") {
        var c = document.createElement("option");
        c.setAttribute("value", "Circulo/Cuadrado");
        var t = document.createTextNode("Círculo/Cuadrado");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Cuadrado/Circulo");
        var t = document.createTextNode("Cuadrado/Círculo");
        c.appendChild(t);
        bandeja.appendChild(c);
    } else if (modooperacion.value === "cc") {
        var c = document.createElement("option");
        c.setAttribute("value", "Verde/Otros");
        var t = document.createTextNode("Verde/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Gris/Otros");
        var t = document.createTextNode("Gris/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Amarillo/Otros");
        var t = document.createTextNode("Amarillo/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Verde");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Gris");
        var t = document.createTextNode("Otros/Gris");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Amarillo");
        var t = document.createTextNode("Otros/Amarillo");
        c.appendChild(t);
        bandeja.appendChild(c);
    }
    bandeja.selected = "0";
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    socket.emit('modo', dispositivo);
    dispositivo = {};
}
function cambiobandejas() {
    var dispositivo = {};
    console.log("Cadenas");
    console.log(cadenas);
    var cadenas = bandeja.value.split("/");
    dispositivo.nombre = "modo";
    dispositivo.modo = modooperacion.value;
    dispositivo.bandeja1 = cadenas[0];
    dispositivo.bandeja2 = cadenas[1];
    socket.emit('modo', dispositivo);
}

socket.on('modo', function (data) {
    var x = bandeja.length;
    for (var i = 0; i <= x; i++) {
        bandeja.options.remove(toString(i));
    }
    if (data.modo === "cmnm") {
        modooperacion.selectedIndex = "0";
        var c = document.createElement("option");
        c.setAttribute("value", "Metal/No metal");
        var t = document.createTextNode("Metal/No metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "No metal/Metal");
        var t = document.createTextNode("No metal/Metal");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "metal/nometal") {
            bandeja.selectedIndex = "0";
        } else {
            bandeja.selectedIndex = "1";
        }
    } else if (data.modo === "cf") {
        modooperacion.selectedIndex = "1";
        var c = document.createElement("option");
        c.setAttribute("value", "Circulo/Cuadrado");
        var t = document.createTextNode("Círculo/Cuadrado");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Cuadrado/Circulo");
        var t = document.createTextNode("Cuadrado/Círculo");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "Circulo/Cuadrado") {
            bandeja.selectedIndex = "0";
        } else {
            bandeja.selectedIndex = "1";
        }

    } else if (data.modo === "cc") {
        modooperacion.selectedIndex = "2";
        var c = document.createElement("option");
        c.setAttribute("value", "Verde/Otros");
        var t = document.createTextNode("Verde/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Gris/Otros");
        var t = document.createTextNode("Gris/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Amarillo/Otros");
        var t = document.createTextNode("Amarillo/Otros");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Verde");
        var t = document.createTextNode("Otros/Verde");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Gris");
        var t = document.createTextNode("Otros/Gris");
        c.appendChild(t);
        bandeja.appendChild(c);
        var c = document.createElement("option");
        c.setAttribute("value", "Otros/Amarillo");
        var t = document.createTextNode("Otros/Amarillo");
        c.appendChild(t);
        bandeja.appendChild(c);
        if (data.bandeja1 + "/" + data.bandeja2 === "Verde/Otros") {
            bandeja.selectedIndex = "0";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Gris/Otros") {
            bandeja.selectedIndex = "1";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Amarillo/Otros") {
            bandeja.selectedIndex = "2";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Verde") {
            bandeja.selectedIndex = "3";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Gris") {
            bandeja.selectedIndex = "4";
        } else if (data.bandeja1 + "/" + data.bandeja2 === "Otros/Amarillo") {
            bandeja.selectedIndex = "5";
        }
    }
});
